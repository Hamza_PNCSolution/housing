    //For Nic
    $('.customercnic , .nomineecnic ').change('change', function () {
        // var idToTest = '12345-1234567-1',
        var checkcinic = $('.customercnic').val();
        var checkcinictwo = $('.nomineecnic').val();
        myRegExp = new RegExp(/\d{5}-\d{7}-\d/);

        if (myRegExp.test(checkcinic)) {
            $('.btn-check').removeAttr('disabled');
            $('.customercnic').removeClass('is-invalid');
        } else {
            $('.customercnic').addClass('is-invalid');
            $('.btn-check').attr('disabled', 'disabled');
        } if (myRegExp.test(checkcinictwo)) {
            $('.btn-check').removeAttr('disabled');
            $('.nomineecnic').removeClass('is-invalid');
        }
        else {
            $('.nomineecnic').addClass('is-invalid');
            $('.btn-check').attr('disabled', 'disabled');
        }
    });

    // For Client Data Appened
        $(".customer").change(function () {
        nameApplicantID = $(this).children(':selected').data('id');
        console.log(nameApplicantID);
        $.get('/salescustomer/'+nameApplicantID,function(data){
            // console.log(data);
            $.each(data, (key, value) => {
                if(key === 'cnic') {
                    $(".customerecnic").addClass('dropify');
                    $(".customerecnic").attr('data-height', '300');
                    $.each(value, (key0, value0) => {
                        console.log(key0, value0);
                        $(".customerecnic").attr('data-default-file', 'storage/' + value0['images']);
                    })

                }
                if(key === 'nominee') {
                    $(".nomineecnic").addClass('dropify');
                    $(".nomineecnic").attr('data-height', '300');
                    $.each(value['cnic'], (key1, value1) => {
                        console.log(key1, value1);
                        $(".nomineecnic").attr('data-default-file', 'storage/' + value1['images']);
                    })
                    $('input[name=relationship]').val(value['relationship']);
                }

                $('input[name='+key+']').val(value);
                $(".dropify").dropify();
            });
        })
    });

    $(".agent").change(function () {
        agentID = $(this).children(':selected').data('id');
        // console.log(agentID);
        $.get('/salesagent/'+agentID,function(data){
            $.each(data, (key, value) => {
                $('input[name='+key+']').val(value);
            });
        })
    });

    $(".plot").change(function () {
        plotID = $(this).children(':selected').data('id');
        // console.log(plotID);
        $.get('/salesplot/'+plotID,function(data){
            // console.log(data);
            $.each(data, (key, value) => {
                
                $('input[name='+key+']').val(value);
            });
        })
    });

    $('.totalcost, .advance').keyup(function(){
    var advance = parseFloat($('.advance').val()) || 0;
    var totalcost = parseFloat($('.totalcost').val()) || 0;
    var total = totalcost - advance;
    $('.remaning_amount').val(total);
    $( ".remaning_amount" ).attr( "readonly", true );
    // console.log(total);
});

    $("#sale_type").change(function () {

        var check = $("#sale_type").val();
        var actualDate = new Date();
        var days = 25;
        actualDate.setDate(actualDate.getDate()+days)
        // console.log(days,actualDate);

        var after45days = new Date();
        after45days.setDate(after45days.getDate()+40)
        // console.log(days,after45days);

        $('.totalcost, .advance').keyup(function(){
            var remaning_amount = $(".remaning_amount").val();
            var inst1_amount = remaning_amount/2;
            console.log(inst1_amount);
            var inst2_amount = remaning_amount - inst1_amount;
            $("#inst1_amount").val(inst1_amount);
            $("#inst2_amount").val(inst2_amount);
        });
        // FOR CASH
        if(check == 'On Cash'){
            $("#on_cash").show();
            $("#on_installment").hide();
            var event = new Date(actualDate);
            let date = JSON.stringify(event)
            date = date.slice(1,11)
            // console.log(date);
            $("#inst1_date").val(date);

            var event1 = new Date(after45days);
            let date1 = JSON.stringify(event1)
            date1 = date1.slice(1,11)
            // console.log(date1);
            $("#inst2_date").val(date1);


        }
        // FOR ISNTALLMENT
        if(check == 'Isntallment'){
            $("#on_installment").show();
            $("#on_cash").hide();
            $('.remaning_amount, #no_installment, #starting_date , #Isntallment_type').on("keyup change",function(){
                var starting_date = $("#starting_date").val();
                var remaning_amount = $(".remaning_amount").val();
                var no_installment = $("#no_installment").val();
                var inst_amount = remaning_amount/no_installment;
                // For Div Empty
                $("#no_installments").empty();
                $("#amount_installment").empty();
                $("#date_installment").empty();
                //For Installment Period
                var check = $("#Isntallment_type").val();
                var dateSrt = new Date(starting_date);
                if(check == 'monthly'){
                    // For Installment Div
                    if(starting_date != "" && no_installment != ""){
                        $("#list_installment").show();
                        // For Appened Input Fileds
                        for (var i = 1; i <= no_installment; i++) {
                            var currentDay = dateSrt.getDate();
                            var currentMonth = dateSrt.getMonth();
                            dateSrt.setMonth(currentMonth + 1, currentDay);
                            if (dateSrt.getMonth() > currentMonth + 1) {
                                dateSrt.setDate(0);
                            }
                            var lastDate = $.datepicker.formatDate('yy-mm-dd', dateSrt);
                            $("#no_installments").append("<div class='input-group'><input type='number' class='form-control' name='no_installments[]' value="+i+" readonly/></div>");
                            $("#amount_installment").append("<div class='input-group'><input type='number' class='form-control' name='amount_installment[]' value="+Math.round(inst_amount)+" readonly/></div>");
                            $("#date_installment").append("<div class='input-group'><input type='date' class='form-control' name='date_installment[]' value="+lastDate+" readonly/></div>");
                            // console.log(inst_amount);
                        }
                    }
                }if(check == 'quarterly'){
                    // For Installment Div
                    if(starting_date != "" && no_installment != ""){
                        $("#list_installment").show();
                        // For Appened Input Fileds
                        for (var i = 1; i <= no_installment; i++) {
                            var currentDay = dateSrt.getDate();
                            var currentMonth = dateSrt.getMonth();
                            dateSrt.setMonth(currentMonth + 3, currentDay);
                            if (dateSrt.getMonth() > currentMonth + 3) {
                                dateSrt.setDate(0);
                            }
                            var lastDate = $.datepicker.formatDate('yy-mm-dd', dateSrt);
                            $("#no_installments").append("<div class='input-group'><input type='number' class='form-control' name='no_installments[]' value="+i+" readonly/></div>");
                            $("#amount_installment").append("<div class='input-group'><input type='number' class='form-control' name='amount_installment[]' value="+Math.round(inst_amount)+" readonly/></div>");
                            $("#date_installment").append("<div class='input-group'><input type='date' class='form-control' name='date_installment[]' value="+lastDate+" readonly/></div>");
                            // console.log(inst_amount);
                        }
                    }
                }if(check == 'half-yearly'){
                    // For Installment Div
                    if(starting_date != "" && no_installment != ""){
                        $("#list_installment").show();
                        // For Appened Input Fileds
                        for (var i = 1; i <= no_installment; i++) {
                            var currentDay = dateSrt.getDate();
                            var currentMonth = dateSrt.getMonth();
                            dateSrt.setMonth(currentMonth + 6, currentDay);
                            if (dateSrt.getMonth() > currentMonth + 6) {
                                dateSrt.setDate(0);
                            }
                            var lastDate = $.datepicker.formatDate('yy-mm-dd', dateSrt);
                            $("#no_installments").append("<div class='input-group'><input type='number' class='form-control' name='no_installments[]' value="+i+" readonly/></div>");
                            $("#amount_installment").append("<div class='input-group'><input type='number' class='form-control' name='amount_installment[]' value="+Math.round(inst_amount)+" readonly/></div>");
                            $("#date_installment").append("<div class='input-group'><input type='date' class='form-control' name='date_installment[]' value="+lastDate+" readonly/></div>");
                            // console.log(inst_amount);
                        }
                    }
                }
            });
        }


    });
