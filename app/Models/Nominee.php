<?php

namespace App\Models;

use App\Models\NomineeCnic;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Nominee extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function client()
    {
        return $this->hasOne(Client::class);
    }
    public function cnic(){
        return $this->hasMany(NomineeCnic::class, 'user_id' , 'id');
    }

}
