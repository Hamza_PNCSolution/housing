<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NomineeCnic extends Model
{
    use HasFactory;
    protected $guarded = [];

    protected $appends = [ 'file' ];

    public function getFileAttribute(){
        return asset($this->images);
    }

}
