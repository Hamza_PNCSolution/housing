<?php

namespace App\Models;

use App\Models\Nominee;
use App\Models\CustomerCnic;
use App\Models\HousingClient;
use App\Models\HousingScheme;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Client extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function nominee(){
        return $this->hasOne(Nominee::class, 'nominee_of');
    }

    public function cnic(){
        return $this->hasMany(CustomerCnic::class, 'user_id' , 'id');
    }

    public function housing(){
        return $this->belongsTo(HousingClient::class,'id','client_id');
    }

    public function manager(){
        return $this->belongsToMany(User::class,'housing_managers','scheme_id','manager_id');
    }

    public function employee(){
        return $this->belongsToMany(User::class,'housing_employees','scheme_id','employee_id');
    }
}
