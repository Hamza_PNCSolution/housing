<?php

namespace App\Models;

use App\Models\ManagerCnic;
use App\Models\EmployeeCnic;
use App\Models\HousingScheme;
use App\Models\HousingManager;
use App\Models\HousingEmployee;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory, Notifiable ,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function usermeta(){
        return $this->belongsTo(UserMeta::class,'id','user_id');
    }

    public function cnic(){
        return $this->hasMany(ManagerCnic::class, 'user_id' , 'id');
    }

    public function employeecnic(){
        return $this->hasMany(EmployeeCnic::class, 'user_id' , 'id');
    }

    public function manager(){
        return $this->belongsTo(HousingManager::class,'id','manager_id');
    }

    public function employee(){
        return $this->belongsTo(HousingEmployee::class,'id','employee_id');
    }
}
