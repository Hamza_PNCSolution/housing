<?php

namespace App\Models;

use App\Models\User;
use App\Models\HousingScheme;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Location extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function scheme(){
        return $this->belongsTo(HousingScheme::class,'scheme_id','id');
    }

    public function users(){
        return $this->belongsTo(User::class,'delete_by','id');
    }
}
