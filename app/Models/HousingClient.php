<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HousingClient extends Model
{
    use HasFactory;
    protected $guarded = [];
}
