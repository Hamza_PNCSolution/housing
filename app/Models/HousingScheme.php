<?php

namespace App\Models;

use App\Models\Location;
use App\Models\HousingManager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class HousingScheme extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function manager(){
        return $this->belongsToMany(User::class,'housing_managers','scheme_id','manager_id');
    }

    public function employee(){
        return $this->belongsToMany(User::class,'housing_employees','scheme_id','employee_id');
    }

    //vendor has many uploaded files
    public function location($type = null)
    {
        $location = $this->hasMany(Location::class,'scheme_id','id');
        if($type != null){
            $location->where(['type' => $type]);
        }

        return $location;
    }
}
