<?php

namespace App\Models;

use App\Models\Plot;
use App\Models\Client;
use App\Models\SaleOncash;
use App\Models\SaleInstallment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Sale extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function client(){
        return $this->hasMany(Client::class,'id','client_id');
    }

    public function plot(){
        return $this->hasMany(Plot::class,'id','plot_id');
    }

    public function oncash(){
        return $this->hasMany(SaleOncash::class,'sale_id','id');
    }

    public function installment(){
        return $this->hasMany(SaleInstallment::class,'sale_id','id');
    }
}
