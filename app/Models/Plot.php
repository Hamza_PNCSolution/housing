<?php

namespace App\Models;

use App\Models\PlotImage;
use App\Models\HousingScheme;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Plot extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function housing(){
        return $this->belongsTo(HousingScheme::class,'housing_scheme_id' , 'id');
    }

    public function images(){
        return $this->hasMany(PlotImage::class,'plot_id' , 'id' );
    }
}
