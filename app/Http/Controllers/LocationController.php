<?php

namespace App\Http\Controllers;

use App\Models\Location;
use Illuminate\Http\Request;
use App\Models\HousingClient;
use App\Models\HousingScheme;
use PhpParser\Node\Expr\FuncCall;
use Illuminate\Support\Facades\Auth;

class LocationController extends Controller
{
    public function index(){

        $current_user = Auth::user();
        $data['page_slug'] = request()->route()->uri();
        $data['scheme'] = Location::with(['scheme','users'])->when( ( $current_user->hasRole('admin') ) ,function ($query) use ($current_user) {})
        ->when( ( $current_user->hasRole('manager') ) ,function ($query) use ($current_user) {
            $query->where(['scheme_id' => auth()->user()->manager->scheme_id, 'delete_at' => '0']);})
        ->when( ( $current_user->hasRole('employee') ) ,function ($query) use ($current_user) {
            $query->where(['scheme_id' => auth()->user()->employee->scheme_id, 'delete_at' => '0']);
        })->get();
        return view('crm.location.index', $data);
    }

    public function create(){

        $current_user = Auth::user();
        $data['page_slug'] = 'location';
        $data['scheme'] = HousingScheme::with('manager')->when( ( $current_user->hasRole('admin') ) ,function ($query) use ($current_user) {})
        ->when( ( $current_user->hasRole('manager') ) ,function ($query) use ($current_user) {
            $query->whereHas('manager', function ($query) use ($current_user) {
                $query->where(['scheme_id' => auth()->user()->manager->scheme_id]);
            });
        })->get();
        return view('crm.location.create', $data);

    }

    public function store(Request $request){

        // dd($request->toArray());
        $location = Location::create(
            [
                'scheme_id' => $request->scheme_id,
                'text' => $request->description,
                'type' => $request->type,
                'created_by' => Auth::id(),
            ]
        );

        if($location->id){
            return redirect('/location')->with(['success' => 'Location is successfully created of scheme']);
            // return back('crm.housing.housing')->with(['status' => "success"], 200);
        }
    }

    public function edit($id)
    {
        $current_user = Auth::user();
        $data['page_slug'] = 'location';
        $data['scheme'] = HousingScheme::with('manager')->when( ( $current_user->hasRole('admin') ) ,function ($query) use ($current_user) {})
        ->when( ( $current_user->hasRole('manager') ) ,function ($query) use ($current_user) {
            $query->whereHas('manager', function ($query) use ($current_user) {
                $query->where(['scheme_id' => auth()->user()->manager->scheme_id]);
            });
        })->get();
        $data['location'] = Location::with('scheme')->find($id);
        return view('crm.location.edit',$data);
    }

    public function update(Request $request)
    {
        // dd($request->toArray());
        $location = Location::find($request->id);
            $location->scheme_id = $request->scheme_id;
            $location->text = $request->description;
            $location->type = $request->type;
            $location->save();
        if($location){
            return redirect('/location')->with(['update' => 'Location is successfully updated']);
        }
    }

    public function destroy($id)
    {
        // dd($id);
        $location = Location::where('id',$id)->update([
            'delete_by' => Auth::id(),
            'delete_at' => '1',
        ]);
        if($location){
            return redirect('/location')->with(['delete' => 'Location is successfully deleted']);
        }
    }

    public function scheme(Request $request){

        // dd($request->toArray());
        $clients = $request->clients;
        foreach($clients as $key => $client){
            $client = HousingClient::create([
                'scheme_id' => $request->scheme,
                'client_id' => $client,
            ]);
        }
        if($client){
            return redirect('/client')->with(['success' => 'Client is assigned to housing scheme']);
        }

    }
}
