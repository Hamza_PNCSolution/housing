<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Manager;
use App\Models\UserMeta;
use App\Models\ManagerCnic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(function($request,$next) {
            $user = Auth::user();
            // dd(Auth::user());
            if($user->hasRole('admin')){
                return $next($request);
            }else{
                return abort(403);
            }
        });

    }


    public function index()
    {
        $data['page_slug'] = request()->route()->uri();
        $data['manager'] = User::with('usermeta')->role('manager')->get();
        // dd($data['manager']);
        return view('crm.manager.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_slug'] = request()->route()->uri();
        return view('crm.manager.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->toArray());
        $managerUser = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make('123'),
        ]);
        $managerUser->assignRole('manager');
        if($managerUser){
            $image_path = $request->file('profile')->store('images/manager/profile', 'public');
            $profile_image = $image_path;
            $manager = UserMeta::create([
                'user_id' => $managerUser->id,
                'profile' => $profile_image,
                'reg_number' => $request->reg_number,
                'father_name' => $request->father_name,
                'data_of_birth' => $request->date_of_birth,
                'gender' => $request->gender,
                'salary' => $request->salary,
                'cnic_no' => $request->managercnic_no,
                'cell_no' => $request->cell,
                'phone_no' => $request->phone,
                'address' => $request->address,
                'city' => $request->city,
                'status' => $request->status,
                'created_by' => auth()->user()->id,

            ]);

            $manager_images = $request->managercnic;
            foreach ($manager_images as $cnic_images) {
                $image_path = $cnic_images->store('images/manager/cnic', 'public');
                $pictures = $image_path;
                $manager_images = ManagerCnic::create([
                    'user_id' => $managerUser->id,
                    'images' => $pictures,
                ]);
            }

            if ($manager_images->id) {
                return redirect('/manager')->with(['success' => 'Managers is successfully created']);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Manager  $manager
     * @return \Illuminate\Http\Response
     */
    public function show(Manager $manager)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Manager  $manager
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_slug'] = 'manager';
        $data['manager'] = User::with(['usermeta','cnic'])->role('manager')->find($id);
        // dd($data['manager']);
        return view('crm.manager.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Manager  $manager
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request->toArray());
        $managerUser = User::updateOrCreate(
            ['id' => $request->id],
            [
                'name' => $request->name,
                'email' => $request->email
            ]
        );
        if($managerUser){
            if ($request->hasFile('profile')) {
                $image_path = $request->file('profile')->store('images/manager/profile', 'public');
                $profile = $image_path;
                $manager = UserMeta::updateOrCreate(
                    ['user_id' => $request->id],
                    [
                        'profile' => $profile,
                        'reg_number' => $request->reg_number,
                        'father_name' => $request->father_name,
                        'data_of_birth' => $request->date_of_birth,
                        'gender' => $request->gender,
                        'cnic_no' => $request->managercnic_no,
                        'salary' => $request->salary,
                        'cell_no' => $request->cell,
                        'phone_no' => $request->phone,
                        'address' => $request->address,
                        'city' => $request->city,
                        'status' => $request->status,
                        'created_by' => auth()->user()->id,
                    ]
                );
            } else {
                $manager = UserMeta::updateOrCreate(
                    ['user_id' => $request->id],
                    [
                        'reg_number' => $request->reg_number,
                        'father_name' => $request->father_name,
                        'data_of_birth' => $request->date_of_birth,
                        'gender' => $request->gender,
                        'salary' => $request->salary,
                        'cnic_no' => $request->managercnic_no,
                        'cell_no' => $request->cell,
                        'phone_no' => $request->phone,
                        'address' => $request->address,
                        'city' => $request->city,
                        'status' => $request->status,
                        'created_by' => auth()->user()->id,
                    ]
                );
            }

            $manager_images = $request->managercnic;
            if ($request->hasFile('managercnic')) {
                ManagerCnic::where('user_id', $request->id)->delete();
                foreach ($manager_images as $manager_img) {
                    $image_path = $manager_img->store('images/manager/cnic', 'public');
                    $pictures = $image_path;
                    $manager_images = ManagerCnic::create([
                        'user_id' => $request->id,
                        'images' => $pictures
                    ]);
                }
            }

            return redirect('/manager')->with(['update' => 'Manager Profile is successfully updated']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Manager  $manager
     * @return \Illuminate\Http\Response
     */
    public function destroy(Manager $manager)
    {
        //
    }
}
