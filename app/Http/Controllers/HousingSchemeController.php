<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\HousingScheme;
use App\Models\HousingManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use PHPUnit\TextUI\XmlConfiguration\CodeCoverage\Report\Html;

class HousingSchemeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(function($request,$next) {
            $user = Auth::user();
            // dd(Auth::user());
            if($user->hasRole('admin')){
                return $next($request);
            }else{
                return abort(403);
            }
        });

    }


    public function index()
    {
        $data['page_slug'] = request()->route()->uri();
        $data['housing'] = HousingScheme::with(['manager','employee'])->where(['delete_at' => '0'])->get();
        return view('crm.housing.housing', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_slug'] = request()->route()->uri();
        $data['manager'] = User::with('usermeta')->role('manager')->get();
        $data['employee'] = User::with('usermeta')->role('employee')->get();
        return view('crm.housing.create-housing' , $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->toArray());
        $user_id = Auth::id();
            $image_path = $request->file('logo')->store('images/logo', 'public');
            $logo = $image_path;
            $housingScheme = HousingScheme::create(
                [
                    'name' => $request->name,
                    'number' => $request->phone,
                    'email' => $request->email,
                    'address' => $request->address,
                    'website' => $request->website,
                    'logo' => $logo,
                    'create_by' => $user_id,
                ]
            );
            $housingScheme->manager()->sync($request->managers);
            $housingScheme->employee()->sync($request->employees);

            if($housingScheme->id){
                return redirect('/housing')->with(['success' => 'Housing Scheme is successfully created']);
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HousingScheme  $housingScheme
     * @return \Illuminate\Http\Response
     */
    public function show(HousingScheme $housingScheme)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HousingScheme  $housingScheme
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_slug'] = 'housing';
        $data['housing'] = HousingScheme::with('manager')->find($id);
        $data['manager'] = User::with('usermeta')->role('manager')->get();
        $data['employee'] = User::with('usermeta')->role('employee')->get();
        return view('crm.housing.update-housing',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HousingScheme  $housingScheme
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request->toArray());
        $housingScheme = HousingScheme::find($request->id);
        if ($request->hasFile('logo')) {
            $image_path = $request->file('logo')->store('images/logo', 'public');
            $logo = $image_path;
            $housingScheme->name = $request->name;
            $housingScheme->number = $request->phone;
            $housingScheme->address = $request->address;
            $housingScheme->email = $request->email;
            $housingScheme->website = $request->website;
            $housingScheme->logo = $logo;
            $housingScheme->save();
        } else {
            $housingScheme->name = $request->name;
            $housingScheme->number = $request->phone;
            $housingScheme->address = $request->address;
            $housingScheme->email = $request->email;
            $housingScheme->website = $request->website;
            $housingScheme->save();
        }
        $housingScheme->manager()->sync($request->manager);
        $housingScheme->employee()->sync($request->employee);
        if($housingScheme){
            return redirect('/housing')->with(['update' => 'Housing Scheme is successfully updated']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HousingScheme  $housingScheme
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $housingScheme = HousingScheme::where('id',$id)->update([
            'delete_by' => Auth::id(),
            'delete_at' => '1',
        ]);
        if($housingScheme){

            return back()->with(['delete' => 'Housing Scheme is successfully Delete']);
        }
    }
}
