<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Nominee;
use App\Models\NomineeCnic;
use App\Models\CustomerCnic;
use Illuminate\Http\Request;
use App\Models\HousingScheme;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $current_user = Auth::user();
        // dd(auth()->user()->manager->scheme_id);
        $data['page_slug'] = request()->route()->uri();
        $data['housing'] = HousingScheme::get();
        $data['client'] = Client::with('housing')->when( ( $current_user->hasRole('admin') ) ,function ($query) use ($current_user) {})
        ->when( ( $current_user->hasRole('manager') ) ,function ($query) use ($current_user) {
            $query->whereHas('housing', function ($query) use ($current_user) {
                $query->where(['scheme_id' => auth()->user()->manager->scheme_id]);
            });})
        ->when( ( $current_user->hasRole('employee') ) ,function ($query) use ($current_user) {
            $query->whereHas('housing', function ($query) use ($current_user) {
                $query->where(['scheme_id' => auth()->user()->employee->scheme_id]);
            });
        })->get();
        // dd($data['client']);
        return view('crm.client.client', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_slug'] = request()->route()->uri();
        return view('crm.client.create-client', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $user_id = auth()->user()->id;
        $image_path = $request->file('profile_customer')->store('images/customer/profile', 'public');
        $profile_image = $image_path;
        $client = Client::create([
            'profile' => $profile_image,
            'name' => $request->customername,
            'father_name' => $request->customerfahtername,
            'gender' => $request->customergender,
            'dob'=> $request->customerdob,
            'cnic_no' => $request->customercnic,
            'cell_no' => $request->customercell,
            'phone_no' => $request->customerphone,
            'email' => $request->customeremail,
            'address' => $request->address,
            'city' => $request->city,
            'created_by' => auth()->user()->id,

        ]);

        $client_images = $request->custumorcnic;
        foreach ($client_images as $cnic_images) {
            $image_path = $cnic_images->store('images/customer/cnic', 'public');
            $pictures = $image_path;
            $client_images = CustomerCnic::create([
                'user_id' => $client->id,
                'images' => $pictures,
            ]);
        }

        $image_path = $request->file('profile_nominee')->store('images/nominee/profile', 'public');
        $profile_image = $image_path;
        $nominee = Nominee::create([
            'profile' => $profile_image,
            'name' => $request->nomineename,
            'father_name' => $request->nomineefathername,
            'gender' => $request->nomineegender,
            'dob'=> $request->nomineedob,
            'cnic_no' => $request->nomineecnicno,
            'cell_no' => $request->nomineecell,
            'phone_no' => $request->nomineephone,
            'relationship' => $request->relationship,
            'nominee_of' => $client->id,
            'created_by' => $user_id,

        ]);

        $nominee_images = $request->nomineecnic;
        foreach ($nominee_images as $cnic_pics) {
            $image_path = $cnic_pics->store('images/nominee/cnic', 'public');
            $pictures = $image_path;
            $nominee_images = NomineeCnic::create([
                'user_id' => $nominee->id,
                'images' => $pictures,
            ]);
        }

        if ($nominee_images->id) {
            return redirect('/client')->with(['success' => 'Client is successfully added']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_slug'] = 'client';
        $data['client'] = Client::find($id);
        return view('crm.client.update-client', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if ($request->hasFile('profile_customer')) {
            $image_path = $request->file('profile_customer')->store('images/customer/profile', 'public');
            $profile = $image_path;
            $client = Client::updateOrCreate(
                ['id' => $request->id],
                [
                    'profile' => $profile,
                    'name' => $request->customername,
                    'father_name' => $request->customerfahtername,
                    'gender' => $request->customergender,
                    'dob'=> $request->customerdob,
                    'cnic_no' => $request->customercnic,
                    'cell_no' => $request->customercell,
                    'phone_no' => $request->customerphone,
                    'email' => $request->customeremail,
                    'address' => $request->address,
                    'city' => $request->city,
                    'created_by' => auth()->user()->id,
                ]
            );
        } else {
            $client = Client::updateOrCreate(
                ['id' => $request->id],
                [
                    'name' => $request->customername,
                    'father_name' => $request->customerfahtername,
                    'gender' => $request->customergender,
                    'dob'=> $request->customerdob,
                    'cnic_no' => $request->customercnic,
                    'cell_no' => $request->customercell,
                    'phone_no' => $request->customerphone,
                    'email' => $request->customeremail,
                    'address' => $request->address,
                    'city' => $request->city,
                    'created_by' => auth()->user()->id,
                ]
            );
        }

        $client = Client::find($request->id);
        $client_images = $request->custumorcnic;
        if ($request->hasFile('custumorcnic')) {
            CustomerCnic::where('user_id', $request->id)->delete();
            foreach ($client_images as $client_img) {
                $image_path = $client_img->store('images/customer/cnic', 'public');
                $pictures = $image_path;
                $client_images = CustomerCnic::create([
                    'user_id' => $request->id,
                    'images' => $pictures
                ]);
            }
        }
        // dd($request->hasFile('profile_nominee'));
        if ($request->hasFile('profile_nominee') == true) {
            $image_path = $request->file('profile_nominee')->store('images/nominee/profile', 'public');
            $profile = $image_path;
            $nominee = Nominee::updateOrCreate(
                ['nominee_of' => $request->id],
                [
                    'profile' => $profile,
                    'name' => $request->nomineename,
                    'father_name' => $request->nomineefathername,
                    'gender' => $request->nomineegender,
                    'dob'=> $request->nomineedob,
                    'cnic_no' => $request->nomineecnicno,
                    'cell_no' => $request->nomineecell,
                    'phone_no' => $request->nomineephone,
                    'relationship' => $request->relationship,
                    'nominee_of' => $client->id,
                    'created_by' => auth()->user()->id,
                ]
            );
        } else {
            $nominee = Nominee::updateOrCreate(
                ['nominee_of' => $request->id],
                [
                    'name' => $request->nomineename,
                    'father_name' => $request->nomineefathername,
                    'gender' => $request->nomineegender,
                    'dob'=> $request->nomineedob,
                    'cnic_no' => $request->nomineecnicno,
                    'cell_no' => $request->nomineecell,
                    'phone_no' => $request->nomineephone,
                    'relationship' => $request->relationship,
                    'created_by' => auth()->user()->id,
                ]
            );
        }

        $nominee_images = $request->nomineecnic;
        if ($request->hasFile('nomineecnic')) {
            NomineeCnic::where('user_id', $request->nominee_id)->delete();
            foreach ($nominee_images as $nominee_img) {
                $image_path = $nominee_img->store('images/nominee/cnic', 'public');
                $pictures = $image_path;
                $nominee_images = NomineeCnic::create([
                    'user_id'=> $request->nominee_id,
                    'images' => $pictures
                ]);
            }
        }

        return redirect('/client')->with(['update' => 'Client is successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::find($id)->delete();
        return back()->with(['delete' => 'Client is successfully Delete']);
    }
}
