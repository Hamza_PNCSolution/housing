<?php

namespace App\Http\Controllers;

use App\Models\Agent;
use App\Models\AgentCnic;
use Illuminate\Http\Request;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_slug'] = request()->route()->uri();
        $data['agent'] = Agent::where('status','yes')->get();
        // dd($data['agent']);
        return view('crm.agent.agent', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_slug'] = request()->route()->uri();
        return view('crm.agent.create-agent', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->toArray());
        $image_path = $request->file('profile')->store('images/agents/profile', 'public');
        $profile_image = $image_path;
        $agent = Agent::create([
            'profile' => $profile_image,
            'reg_number' => $request->reg_number,
            'name' => $request->name,
            'father_name' => $request->father_name,
            'data_of_birth' => $request->date_of_birth,
            'gender' => $request->gender,
            'comission' => $request->commission,
            'cnic_no' => $request->agentcnic_no,
            'cell_no' => $request->cell,
            'phone_no' => $request->phone,
            'email' => $request->email,
            'address' => $request->address,
            'city' => $request->city,
            'status' => $request->status,
            'created_by' => auth()->user()->id,

        ]);

        $agent_images = $request->agentcnic;
        foreach ($agent_images as $cnic_images) {
            $image_path = $cnic_images->store('images/agents/cnic', 'public');
            $pictures = $image_path;
            $agent_images = AgentCnic::create([
                'user_id' => $agent->id,
                'images' => $pictures,
            ]);
        }

        if ($agent_images->id) {
            return redirect('/agent')->with(['success' => 'Agent is successfully added']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function show(Agent $agent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_slug'] = 'agent';
        $data['agent'] = Agent::find($id);
        return view('crm.agent.update-agent', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agent $agent)
    {
        if ($request->hasFile('profile')) {
            $image_path = $request->file('profile')->store('images/agents/profile', 'public');
            $profile = $image_path;
            $agent = Agent::updateOrCreate(
                ['id' => $request->id],
                [
                    'profile' => $profile,
                    'reg_number' => $request->reg_number,
                    'name' => $request->name,
                    'father_name' => $request->father_name,
                    'data_of_birth' => $request->date_of_birth,
                    'gender' => $request->gender,
                    'cnic_no' => $request->agentcnic_no,
                    'comission' => $request->commission,
                    'cell_no' => $request->cell,
                    'phone_no' => $request->phone,
                    'email' => $request->email,
                    'address' => $request->address,
                    'city' => $request->city,
                    'status' => $request->status,
                    'created_by' => auth()->user()->id,
                ]
            );
        } else {
            $agent = Agent::updateOrCreate(
                ['id' => $request->id],
                [
                    'reg_number' => $request->reg_number,
                    'name' => $request->name,
                    'father_name' => $request->father_name,
                    'data_of_birth' => $request->date_of_birth,
                    'gender' => $request->gender,
                    'comission' => $request->commission,
                    'cnic_no' => $request->agentcnic_no,
                    'cell_no' => $request->cell,
                    'phone_no' => $request->phone,
                    'email' => $request->email,
                    'address' => $request->address,
                    'city' => $request->city,
                    'status' => $request->status,
                    'created_by' => auth()->user()->id,
                ]
            );
        }

        $agent = Agent::find($request->id);
        $agent_images = $request->agentcnic;
        if ($request->hasFile('agentcnic')) {
            AgentCnic::where('user_id', $request->id)->delete();
            foreach ($agent_images as $agent_img) {
                $image_path = $agent_img->store('images/agents/cnic', 'public');
                $pictures = $image_path;
                $agent_images = AgentCnic::create([
                    'user_id' => $request->id,
                    'images' => $pictures
                ]);
            }
        }

        return redirect('/agent')->with(['update' => 'Agent is successfully updated']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $agent = Agent::find($id)->update(['status' => 'no']);
        if($agent){
            return redirect('/agent')->with(['delete' => 'Agent is delete successfully']);
        }
    }
}
