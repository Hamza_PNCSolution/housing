<?php

namespace App\Http\Controllers;

use App\Models\Agent;
use App\Models\Client;
use App\Models\Nominee;
use App\Models\Plot;
use App\Models\Sale;
use App\Models\SaleInstallment;
use App\Models\SaleOncash;
use Illuminate\Http\Request;

class SaleController extends Controller
{
    public function index(){

        $data['page_slug'] = request()->route()->uri();
        $data['customer'] = Client::all();
        $data['nominee'] = Nominee::all();
        $data['agent'] = Agent::all();
        $data['plot'] = Plot::all();
        return view('crm.sales.sale' , $data);

    }

    public function customerData($id){

        $data = Client::with('nominee.cnic','cnic')->where('id',$id)->first();
        return response()->json($data);
    }

    public function agentData($id){

        $data = Agent::where('id',$id)->first();
        return response()->json($data);
    }

    public function plotData($id){

        $data = Plot::where('id',$id)->first();
        return response()->json($data);
    }

    public function saveSales(Request $request){
        // dd($request->toArray());
       $sale = Sale::create([
            'file_no' => $request->file_no,
            'client_id' => $request->customer_id,
            'agent_id' => $request->agent_id,
            'plot_id' => $request->plot_no,
            'advance' => $request->advance,
            'remaning_amount' => $request->remaning_amount,
            'type' => $request->sale_type,
        ]);
        if ($sale->id) {
            if($request->sale_type == "On Cash"){
                $no_installment = $request->installment;
                foreach($no_installment as $key => $oncash){
                    $sale_oncash = SaleOncash::create([
                        'sale_id' => $sale->id,
                        'installment' => $request->inst_amount[$key],
                        'date' => $request->inst_date[$key],
                    ]);
                }
            }else{
                $no_installment = $request->no_installments;
                foreach($no_installment as $key => $installment){
                    $sale_oncash = SaleInstallment::create([
                        'sale_id' => $sale->id,
                        'installment' => $request->amount_installment[$key],
                        'date' => $request->date_installment[$key],
                    ]);
                }
            }
            return redirect('/listsales')->with(['success' => 'Sale is successfully added']);
        }
    }

    // For New Sale
    public function getSales(){

        $data['page_slug'] = 'sales';
        $data['sale'] = Sale::with(['client','plot'])->where('is_delete','0')->get();
        // dd($data['sale']);
        return view('crm.sales.list_sale', $data);
    }

    // For View Agreement
    public function viewSales($id){

        $data['page_slug'] = 'sales';
        $data['sale'] = Sale::with(['client.nominee','plot.housing','oncash','installment'])->find($id);
        return view('crm.sales.view-sale', $data);
    }


        // For Installment
    public function installment($id){

        $data['page_slug'] = 'sales';
        $data['sale'] = Sale::with(['client.nominee','plot.housing','oncash','installment'])->find($id);
        return view('crm.sales.installment' , $data);
    }
    public function cashinstallment($id){
        $oncash = SaleOncash::where('id', $id)->update(['paid' => '1']);
        return back()->with('success', 'Installment has been paid succesfully');
    }

    public function cashuninstallment($id){
        $oncash = SaleOncash::where('id', $id)->update(['paid' => '0']);
        return back()->with('delete', 'Installment has been unpaid succesfully');
    }

    public function monthlyinstallment($id){
        $monthly = SaleInstallment::where('id', $id)->update(['paid' => '1']);
        return back()->with('success', 'Installment has been paid succesfully');
    }

    public function monthlyuninstallment($id){
        $monthly = SaleInstallment::where('id', $id)->update(['paid' => '0']);
        return back()->with('delete', 'Installment has been unpaid succesfully');
    }

    // For Sale Delete
    public function deletesale($id){

        $delete = Sale::where('id', $id)->update(['is_delete' => '1']);
        return back()->with('delete', 'Sale has been deleted succesfully');
    }

}
