<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserMeta;
use App\Models\EmployeeCnic;
use App\Models\HousingEmployee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(function($request,$next) {
            $user = Auth::user();
            // dd(Auth::user());
            if($user->hasRole('admin') || $user->hasRole('manager')){
                return $next($request);
            }else{
                return abort(403);
            }
        });

    }

    public function index()
    {
        $current_user = Auth::user();
        $data['page_slug'] = request()->route()->uri();
        $data['employee'] = User::with('usermeta')->role('employee')->when( ( $current_user->hasRole('admin') ) ,function ($query) use ($current_user) {})
        ->when( ( $current_user->hasRole('manager') ) ,function ($query) use ($current_user) {
            // $query->wherehas('scheme_id' ,auth()->user()->manager->scheme_id);
            $query->whereHas('employee', function($query) use ($current_user){
                $query->where('scheme_id' ,auth()->user()->manager->scheme_id);
            });
        })->get();
        return view('crm.employee.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_slug'] = request()->route()->uri();
        return view('crm.employee.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd(auth()->user()->manager->scheme_id);
        // dd($request->toArray());
        if(auth()->user()->hasrole('admin')){
            $employeeUser = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make('123'),
            ]);
            $employeeUser->assignRole('employee');
            if($employeeUser){
                $image_path = $request->file('profile')->store('images/employee/profile', 'public');
                $profile_image = $image_path;
                $employee = UserMeta::create([
                    'user_id' => $employeeUser->id,
                    'profile' => $profile_image,
                    'reg_number' => $request->reg_number,
                    'father_name' => $request->father_name,
                    'data_of_birth' => $request->date_of_birth,
                    'gender' => $request->gender,
                    'salary' => $request->salary,
                    'cnic_no' => $request->employeecnic_no,
                    'cell_no' => $request->cell,
                    'phone_no' => $request->phone,
                    'address' => $request->address,
                    'city' => $request->city,
                    'status' => $request->status,
                    'created_by' => auth()->user()->id,

                ]);

                $employee_images = $request->employeecnic;
                foreach ($employee_images as $cnic_images) {
                    $image_path = $cnic_images->store('images/employee/cnic', 'public');
                    $pictures = $image_path;
                    $employee_images = EmployeeCnic::create([
                        'user_id' => $employeeUser->id,
                        'images' => $pictures,
                    ]);
                }

                if ($employee_images->id) {
                    return redirect('/employee')->with(['success' => 'Employees is successfully created']);
                }
            }
        }else{
            $employeeUser = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make('123'),
            ]);
            $employeeUser->assignRole('employee');
            if($employeeUser){
                $image_path = $request->file('profile')->store('images/employee/profile', 'public');
                $profile_image = $image_path;
                $employee = UserMeta::create([
                    'user_id' => $employeeUser->id,
                    'profile' => $profile_image,
                    'reg_number' => $request->reg_number,
                    'father_name' => $request->father_name,
                    'data_of_birth' => $request->date_of_birth,
                    'gender' => $request->gender,
                    'salary' => $request->salary,
                    'cnic_no' => $request->employeecnic_no,
                    'cell_no' => $request->cell,
                    'phone_no' => $request->phone,
                    'address' => $request->address,
                    'city' => $request->city,
                    'status' => $request->status,
                    'created_by' => auth()->user()->id,

                ]);

                $employee_images = $request->employeecnic;
                foreach ($employee_images as $cnic_images) {
                    $image_path = $cnic_images->store('images/employee/cnic', 'public');
                    $pictures = $image_path;
                    $employee_images = EmployeeCnic::create([
                        'user_id' => $employeeUser->id,
                        'images' => $pictures,
                    ]);
                }

                if ($employee_images->id) {

                    $housingemployee = HousingEmployee::create([
                        'scheme_id' => auth()->user()->manager->scheme_id,
                        'employee_id' => $employeeUser->id,
                    ]);

                    return redirect('/employee')->with(['success' => 'Employees is successfully created']);
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_slug'] = 'employee';
        $data['employee'] = User::with(['usermeta','employeecnic'])->role('employee')->find($id);
        // dd($data['employee']);
        return view('crm.employee.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request->toArray());
        $employeeUser = User::updateOrCreate(
            ['id' => $request->id],
            [
                'name' => $request->name,
                'email' => $request->email
            ]
        );
        if($employeeUser){
            if ($request->hasFile('profile')) {
                $image_path = $request->file('profile')->store('images/employee/profile', 'public');
                $profile = $image_path;
                $employee = UserMeta::updateOrCreate(
                    ['user_id' => $request->id],
                    [
                        'profile' => $profile,
                        'reg_number' => $request->reg_number,
                        'father_name' => $request->father_name,
                        'data_of_birth' => $request->date_of_birth,
                        'gender' => $request->gender,
                        'cnic_no' => $request->employeecnic_no,
                        'salary' => $request->salary,
                        'cell_no' => $request->cell,
                        'phone_no' => $request->phone,
                        'address' => $request->address,
                        'city' => $request->city,
                        'status' => $request->status,
                        'created_by' => auth()->user()->id,
                    ]
                );
            } else {
                $employee = UserMeta::updateOrCreate(
                    ['user_id' => $request->id],
                    [
                        'reg_number' => $request->reg_number,
                        'father_name' => $request->father_name,
                        'data_of_birth' => $request->date_of_birth,
                        'gender' => $request->gender,
                        'salary' => $request->salary,
                        'cnic_no' => $request->employeecnic_no,
                        'cell_no' => $request->cell,
                        'phone_no' => $request->phone,
                        'address' => $request->address,
                        'city' => $request->city,
                        'status' => $request->status,
                        'created_by' => auth()->user()->id,
                    ]
                );
            }

            $employee_images = $request->employeecnic;
            if ($request->hasFile('employeecnic')) {
                EmployeeCnic::where('user_id', $request->id)->delete();
                foreach ($employee_images as $employee_img) {
                    $image_path = $employee_img->store('images/employee/cnic', 'public');
                    $pictures = $image_path;
                    $employee_images = EmployeeCnic::create([
                        'user_id' => $request->id,
                        'images' => $pictures
                    ]);
                }
            }

            return redirect('/employee')->with(['update' => 'Employee Profile is successfully updated']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
    }
}
