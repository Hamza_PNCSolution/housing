<?php

namespace App\Http\Controllers;

use PDF;
use App\Models\Plot;
use Illuminate\Http\Request;

class EmpController extends Controller
{
    public function download(){

        $data['page_slug'] = 'plot';
        $data['plot'] = Plot::find(13);
        $pdf = PDF::loadview('crm.plot.view-plot' , $data);
        return $pdf->download('plot.pdf');
    }
}
