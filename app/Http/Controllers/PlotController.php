<?php

namespace App\Http\Controllers;

use App\Models\Plot;
use App\Models\Location;
use App\Models\PlotImage;
use Illuminate\Http\Request;
use App\Models\HousingScheme;
use Illuminate\Support\Facades\Auth;

class PlotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $current_user = Auth::user();
        $data['page_slug'] = request()->route()->uri();
        $data['plot_detials'] = Plot::with('housing.manager')->when( ( $current_user->hasRole('admin') ) ,function ($query) use ($current_user) {})
        ->when( ( $current_user->hasRole('manager') ) ,function ($query) use ($current_user) {
            $query->whereHas('housing.manager', function ($query) use ($current_user) {
                $query->where(['manager_id' => auth()->user()->manager->manager_id]);
            });})
        ->when( ( $current_user->hasRole('employee') ) ,function ($query) use ($current_user) {
            $query->whereHas('housing.employee', function ($query) use ($current_user) {
                $query->where(['employee_id' => auth()->user()->employee->employee_id]);
            });
        })->get();
        return view('crm.plot.plot', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $current_user = Auth::user();
        $data['page_slug'] = request()->route()->uri();
        $data['plot'] = HousingScheme::with('location')->when( ( $current_user->hasRole('admin') ) ,function ($query) use ($current_user) {})
        ->when( ( $current_user->hasRole('manager') ) ,function ($query) use ($current_user) {
            $query->whereHas('manager', function ($query) use ($current_user) {
                $query->where(['scheme_id' => auth()->user()->manager->scheme_id]);
            });
        })->where('delete_at','0')->get();
        return view('crm.plot.create-plot', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $plot = Plot::create([
            'plot_id' => $request->plot_id,
            'housing_scheme_id' => $request->housingscheme_id,
            'type' => $request->type,
            'block' => $request->block,
            'phase' => $request->phase,
            'width' => $request->width,
            'length' => $request->length,
            'sqrft' => $request->sqrft,
            'sqrmt' => $request->sqrmt,
            'marla' => $request->marla,
            'cost' => $request->cost,
            'totalcost' => $request->cost,
            'khewat_no' => $request->khewat_no,
            'khasra_no' => $request->khasra_no,
            'status' => $request->status,
            'location' => $request->location,
            'address' => $request->address,
            'create_by' => auth()->user()->id,

        ]);

        $plot_images = $request->plotpictures;
        if($plot_images){
            foreach ($plot_images as $plot_images) {
                $image_path = $plot_images->store('images/plot', 'public');
                $pictures = $image_path;
                $plot_images = PlotImage::create([
                    'plot_id' => $plot->id,
                    'pictures' => $pictures,
                ]);
            }
        }


        if ($plot) {
            return redirect('/plot')->with(['success' => 'Plot is successfully added']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Plot  $plot
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['page_slug'] = 'plot';
        $data['plot'] = Plot::with('housing')->find($id);
        // dd($data['plot']);
        return view('crm.plot.view-plot', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Plot  $plot
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $current_user = Auth::user();
        $data['page_slug'] = 'plot';
        $data['housing'] = HousingScheme::with('location')->when( ( $current_user->hasRole('admin') ) ,function ($query) use ($current_user) {})
        ->when( ( $current_user->hasRole('manager') ) ,function ($query) use ($current_user) {
            $query->whereHas('manager', function ($query) use ($current_user) {
                $query->where(['scheme_id' => auth()->user()->manager->scheme_id]);
            });
        })->where('delete_at','0')->get();
        $data['plot'] = Plot::find($id);
        return view('crm.plot.update-plot', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Plot  $plot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request->toArray());
        $plot = Plot::find($request->id);
        $plot_images = $request->plotpictures;
        if ($request->hasFile('plotpictures')) {
            PlotImage::where('plot_id', $request->id)->delete();
            foreach ($plot_images as $plot_img) {
                $image_path = $plot_img->store('images/plot', 'public');
                $pictures = $image_path;
                // dump($pictures);
                $plot_images = PlotImage::create([
                    'plot_id' => $request->id,
                    'pictures' => $pictures
                ]);
            }
        }
        $plot->plot_id = $request->plot_id;
        $plot->housing_scheme_id = $request->housingscheme_id;
        $plot->type = $request->type;
        $plot->block = $request->block;
        $plot->phase = $request->phase;
        $plot->width = $request->width;
        $plot->length = $request->length;
        $plot->sqrft = $request->sqrft;
        $plot->sqrmt = $request->sqrmt;
        $plot->marla = $request->marla;
        $plot->cost = $request->cost;
        $plot->totalcost = $request->plotcost;
        $plot->khewat_no = $request->khewat_no;
        $plot->khasra_no = $request->khasra_no;
        $plot->status = $request->status;
        $plot->location = $request->location;
        $plot->address = $request->address;
        $plot->create_by = auth()->user()->id;
        $plot->save();
        return redirect('/plot')->with(['update' => 'Plot is successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Plot  $plot
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $plot_img = PlotImage::where('plot_id', $id)->delete();
        $plot = Plot::find($id)->delete();
        return back()->with(['delete' => 'Plot is successfully Delete']);
    }
}
