<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware('auth')->group(function () {
    Route::get('/dashboard', function () {
        $data['page_slug'] = request()->route()->uri();
        return view('crm.dashboard', $data);
    })->name('dashboard');

    Route::group(['middlerware' => 'role:Admin'], function() {

        Route::resource('/plot', 'PlotController');
        Route::resource('/client', 'ClientController');
        Route::resource('/agent', 'AgentController');
        Route::resource('/manager', 'ManagerController');
        Route::resource('/employee', 'EmployeeController');
        Route::resource('/housing', 'HousingSchemeController');

        // For Customer Assigned to Housing Scheme
        Route::post('/scheme', 'LocationController@scheme')->name('store.scheme');


        //For locations of schemes
        Route::get('/location', 'LocationController@index')->name('get.location');
        Route::get('/createlocation', 'LocationController@create')->name('create.location');
        Route::post('/storelocation', 'LocationController@store')->name('store.location');
        Route::get('/editlocation/{id}', 'LocationController@edit')->name('edit.location');
        Route::post('/updatelocation', 'LocationController@update')->name('update.location');
        Route::get('/deletelocation/{id}', 'LocationController@destroy')->name('delete.location');

        //For Sales Agreement
        Route::get('/sales', 'SaleController@index')->name('get.sale');
        Route::get('/listsales', 'SaleController@getSales')->name('list.sale');
        Route::get('/salescustomer/{id}', 'SaleController@customerData')->name('sale.customerData');
        Route::get('/salesagent/{id}', 'SaleController@agentData')->name('sale.agentData');
        Route::get('/salesplot/{id}', 'SaleController@plotData')->name('sale.plotData');
        Route::get('/viewsales/{id}', 'SaleController@viewSales')->name('view.sale');
        Route::post('/savesales', 'SaleController@saveSales')->name('save.sale');

        // For Installment View
        Route::get('/installmentpay/{id}', 'SaleController@installment')->name('installment.pay');

        // For Cash Installment
        Route::post('/cashinstallment/{id}', 'SaleController@cashinstallment')->name('installment.cash');
        Route::post('/cashuninstallment/{id}', 'SaleController@cashuninstallment')->name('uninstallment.cash');

        // For Monthly Installment
        Route::post('/monthlyinstallment/{id}', 'SaleController@monthlyinstallment')->name('installment.monthly');
        Route::post('/monthlyuninstallment/{id}', 'SaleController@monthlyuninstallment')->name('uninstallment.monthly');

        // For Sale Delete
        Route::get('/delete/{id}', 'SaleController@deletesale')->name('sale.delete');

        Route::get('/download', 'EmpController@download')->name('download');
    });

});


require __DIR__.'/auth.php';
