@extends('crm.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            Update Agent
        </h3>
    </div>
    <!--begin::Form-->
    <form method="POST" action="{{route('agent.update', $agent->id)}}" enctype="multipart/form-data" class="form">
        @csrf
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{$agent->id}}"/>
        <div class="card-body">
            <div class="form-group row">
                <div class="col-lg-12">
                    <h6>Profile Picture: <span class="text-danger font-weight-bolder font-size-lg">*</span></h6>
                    <div class="image-input image-input-outline" id="kt_image_4"
                        style="background-image: url({{ asset('crm/assets/media/users/blank.png') }}) ">

                        <div class="image-input-wrapper"
                            style="background-image: url({{ asset('/storage/'.$agent->profile) }}) "></div>

                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                            data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                            <i class="fa fa-pen icon-sm text-muted"></i>
                            <input type="file" name="profile" accept=".png, .jpg, .jpeg" />
                            <input type="hidden" name="profile_avatar_remove" />
                        </label>

                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                            data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                        </span>

                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                            data-action="remove" data-toggle="tooltip" title="Remove avatar">
                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Reg Number:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="number" name="reg_number" class="form-control"
                        placeholder="Enter reg number" value="{{$agent->reg_number}}" required/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Name:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="text" name="name" class="form-control"
                        placeholder="Enter name" value="{{$agent->name}}" required/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Father Name:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="text" name="father_name" class="form-control"
                        placeholder="Enter father name" value="{{$agent->father_name}}" required/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Date of Birth:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="date" name="date_of_birth" class="form-control"
                        placeholder="Enter father name" value="{{$agent->data_of_birth}}" required/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Commission Ratio:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="number" placeholder="Enter Comission Ratio" step="0.001" name="commission" class="form-control" required="" value="{{$agent->comission}}" required/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Gender:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <select type="date" name="gender" class="form-control" required>
                        <option value="">Please Select Gender</option>
                        <option value="male" {{ ($agent->gender == 'male') ? 'selected': null }}>Male</option>
                        <option value="female" {{ ($agent->gender == 'female') ? 'selected': null }}>Female</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>C.N.I.C No:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="text" data-inputmask="'mask': '99999-9999999-9'" placeholder="XXXXX-XXXXXXX-X"
                        name="agentcnic_no" class="form-control agentcnic " required="" value="{{$agent->cnic_no}}" required/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>C.N.I.C Picture:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="file" class="dropify" data-height="200" data-allowed-file-extensions="png jpg jpeg" name="agentcnic[]"
                        data-errors-position="outside" data-max-file-size-preview="2M" data-show-errors="true" multiple data-default-file="{{ asset('/storage/'.$agent->cnic[0]->images) }}"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Cell No:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="tel" name="cell" class="form-control"
                        placeholder="Enter full cell nnumber" required value="{{$agent->cell_no}}" required/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Phone No:</label>
                    <input type="tel" name="phone" class="form-control"
                        placeholder="Enter full phone number" required value="{{$agent->phone_no}}"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Email<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="email" name="email" class="form-control"
                        placeholder="Enter full phone number" required value="{{$agent->email}}" required/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Address:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="address" placeholder="Enter address" value="{{$agent->address}}" required/>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>City:</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="city" placeholder="Enter city" value="{{$agent->city}}"/>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Status:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <select type="date" name="status" class="form-control" required>
                        <option value="">Please Select Status</option>
                        <option value="yes" {{ ($agent->status == 'yes') ? 'selected': null }} >Yes</option>
                        <option value="no" {{ ($agent->status == 'no') ? 'selected': null }} >No</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row text-center">
                <div class="col-lg-2">
                    <button type="submit" class="btn btn-primary btn-lg btn-block btn-check">Save</button>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection
@section('footer.script')
<script>
    var avatar4 = new KTImageInput('kt_image_4');

    avatar4.on('cancel', function (imageInput) {
        swal.fire({
            title: 'Image successfully changed !',
            type: 'success',
            buttonsStyling: false,
            confirmButtonText: 'Awesome!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
    });

    avatar4.on('change', function (imageInput) {
        swal.fire({
            title: 'Image successfully changed !',
            type: 'success',
            buttonsStyling: false,
            confirmButtonText: 'Awesome!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
    });

    avatar4.on('remove', function (imageInput) {
        swal.fire({
            title: 'Image successfully removed !',
            type: 'error',
            buttonsStyling: false,
            confirmButtonText: 'Got it!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
    });

    $('.agentcnic').change('change', function () {
        // var idToTest = '12345-1234567-1',
        var checkcinic = $('.agentcnic').val();
        myRegExp = new RegExp(/\d{5}-\d{7}-\d/);

        if (myRegExp.test(checkcinic)) {
            $('.btn-check').removeAttr('disabled');
            $('.agentcnic').removeClass('is-invalid');
        } else {
            $('.agentcnic').addClass('is-invalid');
            $('.btn-check').attr('disabled', 'disabled');
        }
    });

</script>
@endsection
