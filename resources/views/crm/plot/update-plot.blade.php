@extends('crm.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            Update Plot
        </h3>
    </div>
    <!--begin::Form-->
    <form action="{{ route('plot.update', $plot->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <div class="card-body">
            <div class="form-group row">
                <label class="col-2 col-form-label">Plot Id</label>
                <div class="col-10">
                    <input class="form-control" type="hidden" name="id" value="{{$plot->id}}"/>
                    <input class="form-control" type="text" name="plot_id" id="example-text-input" placeholder="Enter plot id" value="{{$plot->plot_id}}"/>
                </div>
            </div>

            <div class="form-group row">
                @hasrole('admin')
                    <label class="col-2 col-form-label">Housing Scheme<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                        <div class="col-10">
                            <select class="form-control" name="housingscheme_id" id="example-text-input" required>
                                <option value="">Please Select Housing Scheme</option>
                                @foreach ($housing as $data)
                                    <option value="{{ $data->id }}" {{ ($data->id == $plot->housing_scheme_id) ? 'selected' : null  }}>{{$data->name}}</option>
                                @endforeach
                            </select>
                        </div>
                @else
                    <label class="col-2 col-form-label">Housing Scheme<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                        <div class="col-10">
                            <input class="form-control" type="text" name="scheme_name" value="{{ $housing[0]->name ?? '' }}"  readonly/>
                            <input class="form-control" type="hidden" name="housingscheme_id" value="{{ $housing[0]->id ?? '' }}"/>
                        </div>
                @endhasrole
            </div>

            <div class="form-group row">
                <label class="col-2 col-form-label">Plot Type <span class="text-danger font-weight-bolder">*</span></label>
                <div class="col-10">
                    <select class="form-control" id="exampleSelect1" name="type" required>
                        <option value="">Select Plot Type</option>
                        <option {{ ($plot->type) == 'Commercial' ? 'selected' : '' }} value="Commercial">Commercial</option>
                        <option {{ ($plot->type) == 'Residential' ? 'selected' : '' }} value="Residential">Residential</option>
                        <option {{ ($plot->type) == 'Renteral' ? 'selected' : '' }} value="Renteral">Renteral</option>
                        <option {{ ($plot->type) == 'Villa' ? 'selected' : '' }} value="Villa">Villa</option>
                        <option {{ ($plot->type) == 'House' ? 'selected' : '' }} value="House">House</option>
                    </select>
                </div>
            </div>


            <div class="form-group row">
                <label class="col-2 col-form-label">Block <span class="text-danger font-weight-bolder">*</span></label>
                <div class="col-10">
                    <select class="form-control" name="block" required>
                        <option value="">Select Block of Plot</option>
                        @foreach ($housing as $block)
                            @foreach ($block->location('block')->get() as $location)
                                <option value="{{$location->text}}">{{ $location->text }}</option>
                            @endforeach
                        @endforeach
                    </select>
                </div>
            </div>


            <div class="form-group row">
                <label class="col-2 col-form-label">Phase <span class="text-danger font-weight-bolder">*</span></label>
                <div class="col-10">
                    <select class="form-control" name="phase" required>
                        <option value="">Select Phase of Plot</option>
                        @foreach ($housing as $block)
                            @foreach ($block->location('phase')->get() as $location)
                                <option value="{{$location->text}}">{{ $location->text }}</option>
                            @endforeach
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Size <span class="text-danger font-weight-bolder">*</span></label>
                <div class="col-2">
                    <input class="form-control width" type="number" name="width" id="example-number-input" placeholder="Enter width" value="{{$plot->width}}" required/>
                </div>
                <div class="col-2">
                    <input class="form-control length" type="number" name="length" id="example-number-input" placeholder="Enter length" value="{{$plot->length}}" required/>
                </div>
                <div class="col-3">
                    <input class="form-control sqrft" type="number" name="sqrft" id="example-number-input" placeholder="Sqr ft" value="{{$plot->sqrft}}" autocomplete="off"/>
                </div>

                <div class="col-3">
                    <input class="form-control sqrmt" type="number" name="sqrmt" id="example-number-input" placeholder="Sqr mt" value="{{$plot->sqrmt}}" autocomplete="off"/>
                </div>
            </div>

            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Cost <span class="text-danger font-weight-bolder">*</span></label>
                <div class="col-3">
                    <input class="form-control marla" type="number" name="marla" id="example-text-input" placeholder="Enter marla" value="{{$plot->marla}}" required/>
                </div>
                <div class="col-3">
                    <input class="form-control cost" type="number" name="cost" id="example-text-input" placeholder="Enter cost per marla" value="{{$plot->cost}}" required/>
                </div>
                <div class="col-4">
                    <input class="form-control plotcost" type="number" name="plotcost" id="example-text-input" placeholder="Plot Cost" value="{{$plot->totalcost}}" autocomplete="off"/>
                </div>
            </div>

            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Khewat No</label>
                <div class="col-10">
                    <input class="form-control" type="text" name="khewat_no" id="example-number-input" placeholder="Enter khewat number" value="{{$plot->khewat_no}}"/>
                </div>
            </div>

            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Khasra No.</label>
                <div class="col-10">
                    <input class="form-control" type="text" name="khasra_no" id="example-number-input" placeholder="Enter khasra number" value="{{$plot->khasra_no}}"/>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-2 col-form-label">Status <span class="text-danger font-weight-bolder">*</span></label>
                <div class="col-10">
                    <select class="form-control" id="exampleSelect1" name="status" required>
                        <option value="">Select Status</option>
                        <option {{ ($plot->status) == 'Booked' ? 'selected' : '' }} value="Booked">Booked</option>
                        <option {{ ($plot->status) == 'Unbooked/Available' ? 'selected' : '' }} value="Unbooked/Available">Unbooked/Available</option>
                        <option {{ ($plot->status) == 'Blocked' ? 'selected' : '' }} value="Blocked">Blocked</option>
                        <option {{ ($plot->status) == 'Mortgage' ? 'selected' : '' }} value="Mortgage">Mortgage</option>
                        <option {{ ($plot->status) == 'Renteral' ? 'selected' : '' }} value="Renteral">Renteral</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-2 col-form-label">Location <span class="text-danger font-weight-bolder">*</span></label>
                <div class="col-10">
                    <select class="form-control" id="exampleSelect1" name="location" required>
                        <option value="">Select Location</option>
                        <option {{ ($plot->location) == 'main boleward' ? 'selected' : '' }} value="main boleward">Main Boleward </option>
                        <option {{ ($plot->location) == 'main road' ? 'selected' : '' }} value="main road">Main Road</option>
                        <option {{ ($plot->location) == 'park facing' ? 'selected' : '' }} value="park facing">Park Facing</option>
                        <option {{ ($plot->location) == 'corner' ? 'selected' : '' }} value="corner">Corner</option>
                        <option {{ ($plot->location) == 'bublick building facing' ? 'selected' : '' }} value="bublick building facing">Bublick Building Facing</option>
                        @foreach ($housing as $block)
                            @foreach ($block->location('location')->get() as $location)
                                <option value="{{$location->text}}">{{ $location->text }}</option>
                            @endforeach
                        @endforeach
                    </select>
                </div>
            </div>


            <div class="form-group row">
                <label class="col-2 col-form-label">Address</label>
                <div class="col-10">
                    <textarea class="form-control" type="address" name="address" id="example-text-input" rows="5" required>{{$plot->address}}
                    </textarea>
                </div>
            </div>


            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Pictures</label>
                <div class="col-10">
                    <input type="file" class="dropify" name="plotpictures[]" data-default-file="{{ (count($plot->images) != 0) ? asset('/storage/'.$plot->images[0]->pictures) : null }}" data-height="200" data-allowed-file-extensions="pdf png psd" data-errors-position="outside" data-show-errors="true" multiple/>
                </div>
            </div>

        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-2">
                    <button type="submit" class="btn btn-success btn-lg btn-block">Update</button>
                </div>
                <div class="col-10">
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
@section('footer.script')
<script>
// For Size Calculate
$('.width , .length').change(function(){
    var width = parseFloat($('.width').val()) || 0;
    var length = parseFloat($('.length').val()) || 0;
    var sqft = width * length;
    var ft = sqft.toFixed(2);
    $('.sqrft').val(ft);
    var sqmt = ft / 10.764;
    var mt = sqmt.toFixed(2);
    $('.sqrmt').val(mt);
    $( ".sqrft , .sqrmt" ).attr( "readonly", true );
});

// For Plot Cost
$('.marla, .cost').change(function(){
    var rate = parseFloat($('.marla').val()) || 0;
    var box = parseFloat($('.cost').val()) || 0;
    var totalcost = rate * box;
    $('.plotcost').val(totalcost);
    $( ".plotcost" ).attr( "readonly", true );
    console.log(totalcost);
});

</script>
@endsection
