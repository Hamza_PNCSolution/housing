@extends('crm.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Plots
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            <a href="{{ route('plot.create') }}" class="btn btn-primary font-weight-bolder">
                <span class="svg-icon svg-icon-md">
                    <i class="fa fa-plus-circle"></i>
                </span>New Record</a>
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..."
                                    id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--end: Search Form-->
        <!--begin: Datatable-->
        <div class="panel panel-default">
            @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @elseif (session('delete'))
                <div class="alert alert-danger">{{ session('delete') }}</div>
            @elseif (session('update'))
                <div class="alert alert-success">{{ session('update') }}</div>
            @endif
        </div>
        <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
            <thead>
                <tr>
                    <th >Sr No</th>
                    <th >Plot Id</th>
                    <th >Housing Scheme</th>
                    <th >Plot Type</th>
                    <th >Cost</th>
                    <th >Action</th>
                </tr>
            </thead>
            <tbody>

                <?php $x = 0 ?>
                @foreach ($plot_detials as $item)
                <tr>
                    <td>{{ $x+1 }}</td>
                    <td>{{ $item->plot_id ?? 'Not Assigned'  }}</td>
                    <td>{{ $item->housing->name}}</td>
                    <td class="text-right">{{ $item->type }}</td>
                    <td class="text-right">Rs {{ $item->totalcost }}</td>
                    <td data-field="Actions" data-autohide-disabled="false" aria-label="null" class="datatable-cell">
                        <span style="overflow: visible; position: relative; width: 125px;">
                            <a href="{{ route('plot.show',$item->id) }}" class="btn btn-sm btn-clean btn-icon mr-2" title="View details">
                                <span class="svg-icon svg-icon-md">
                                    <i class="fas fa-eye"></i>
                                </span>
                            </a>
                            <a href="{{ route('plot.edit',$item->id) }}" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">
                                <span class="svg-icon svg-icon-md">
                                    <i class="fas fa-pen"></i>
                                </span>
                            </a>
                            <form method="Post" action="{{ route('plot.destroy',$item->id) }}" style="display: -webkit-inline-box;">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-clean btn-icon" title="Delete">
                                    <span class="svg-icon svg-icon-md">
                                        <i class="fas fa-trash"></i>
                                    </span>
                                </button>
                            </form>
                        </span>
                    </td>
                </tr>
                <?php $x++ ?>
                @endforeach
            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
@endsection
