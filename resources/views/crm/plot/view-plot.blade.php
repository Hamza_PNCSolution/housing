@extends('crm.layouts.app')
@section('content')
<div class="card card-custom overflow-hidden">
    <div class="card-body p-0">
        <!-- begin: Invoice-->
        <!-- begin: Invoice header-->
        <div class="row justify-content-center py-8 px-8 py-md-12 px-md-0">
            <div class="col-md-10">
                <div class="d-flex justify-content-between pb-10 pb-md-10 flex-column flex-md-row">
                    <h1 class="display-4 font-weight-boldest mb-10">{{$plot->housing->name}}</h1>
                    <div class="d-flex flex-column align-items-md-end px-0">
                        <!--begin::Logo-->
                        <a href="#" class="mb-5">
                            <img src="{{ asset('storage/'.$plot->housing->logo) }}" alt="Hosuing Logo" height="96px"
                                width="96px" />
                        </a>
                        <!--end::Logo-->
                        <span class="d-flex flex-column align-items-md-end opacity-70">
                            <span>{{$plot->housing->address}}</span>
                            <span>{{$plot->housing->number}}</span>
                        </span>
                    </div>
                </div>
                <div class="border-bottom w-100"></div>
                <div class="d-flex justify-content-between pt-6">
                    <div class="d-flex flex-column flex-root">
                        <span class="font-weight-bolder mb-2">Created At</span>
                        <span class="opacity-70">{{$plot->created_at->format('Y-m-d')}}</span>
                    </div>
                    <div class="d-flex flex-column flex-root">
                        <span class="font-weight-bolder mb-2">TYPE</span>
                        <span class="text-primary font-weight-bolder opacity-70">{{$plot->type}}</span>
                    </div>
                    <div class="d-flex flex-column flex-root">
                        <span class="font-weight-bolder mb-2">Location</span>
                        <span class="text-primary font-weight-bolder opacity-70">{{strtoupper($plot->location)}}</span>
                    </div>
                </div>
                <div class="d-flex justify-content-between pt-6">
                    <div class="d-flex flex-column flex-root">
                        <span class="font-weight-bolder mb-2">BLOCK</span>
                        <span class="opacity-70">{{$plot->block}}</span>
                    </div>
                    <div class="d-flex flex-column flex-root">
                        <span class="font-weight-bolder mb-2">PHASE</span>
                        <span class="opacity-70">{{$plot->phase}}</span>
                    </div>
                    <div class="d-flex flex-column flex-root">
                        <span class="font-weight-bolder mb-2">Address</span>
                        <span class="text-primary font-weight-bolder opacity-70">{{$plot->address}}</span>
                    </div>
                </div>

            </div>
        </div>
        <!-- end: Invoice header-->
        <!-- begin: Invoice body-->

        <div class="row justify-content-center py-8 px-8 py-md-8 px-md-0">
            <div class="col-md-10">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="pl-0 font-weight-bold text-muted text-uppercase">Size Description</th>
                                <th class="text-right font-weight-bold text-muted text-uppercase">Width</th>
                                <th class="text-right font-weight-bold text-muted text-uppercase">Length</th>
                                <th class="text-right pr-0 font-weight-bold text-muted text-uppercase">Sq Ft</th>
                                <th class="text-right pr-0 font-weight-bold text-muted text-uppercase">Sq Mt</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr class="font-weight-boldest">
                                <td class="pl-0 pt-5">Size</td>
                                <td class="text-right pt-5">{{$plot->width}}</td>
                                <td class="text-right pt-5">{{$plot->length}}</td>
                                <td class="text-danger pr-0 pt-5 text-right">{{$plot->sqrft}}</td>
                                <td class="text-danger pr-0 pt-5 text-right">{{$plot->sqrmt}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- end: Invoice body-->
        <!-- begin: Invoice footer-->
        <div class="row justify-content-center bg-gray-100 py-8 px-8 py-md-10 px-md-0">
            <div class="col-md-10">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="font-weight-bold text-muted text-uppercase">PLOT ID</th>
                                <th class="font-weight-bold text-muted text-uppercase">MARLA</th>
                                <th class="font-weight-bold text-muted text-uppercase">COST PER MARLA</th>
                                <th class="font-weight-bold text-muted text-uppercase">TOTAL COST</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="font-weight-bolder">
                                <td>{{$plot->plot_id}}</td>
                                <td>{{$plot->marla}}</td>
                                <td>{{$plot->cost}}</td>
                                <td class="text-danger font-size-h3 font-weight-boldest">{{$plot->totalcost}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- end: Invoice footer-->
        <!-- begin: Invoice action-->
        <div class="row justify-content-center py-8 px-8 py-md-10 px-md-0">
            <div class="col-md-10">
                <div class="d-flex justify-content-between">
                    <a href="#" type="button" class="btn btn-primary font-weight-bold" id="btnprint"
                        onclick="print_page()">Print Invoice</a>
                </div>
            </div>
        </div>
        <!-- end: Invoice action-->
        <!-- end: Invoice-->
    </div>
</div>
@endsection
@section('footer.script')
<script>
    function print_page() {
        var ButtonControl = document.getElementById("btnprint");
        ButtonControl.style.visibility = "hidden";
        window.print();
    }
</script>
@endsection
