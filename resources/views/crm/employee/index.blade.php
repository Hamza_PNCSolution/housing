@extends('crm.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">All Employee Detials
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            <a href="{{ route('employee.create') }}" class="btn btn-primary font-weight-bolder">
                <span class="svg-icon svg-icon-md">
                    <i class="fa fa-plus-circle"></i>
                </span>New Record</a>
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..."
                                    id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--end: Search Form-->
        <!--begin: Datatable-->
        <div class="panel panel-default">
            @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @elseif (session('delete'))
                <div class="alert alert-danger">{{ session('delete') }}</div>
            @elseif (session('update'))
                <div class="alert alert-success">{{ session('update') }}</div>
            @endif
        </div>
        <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
            <thead>
                <tr>
                    <th >#</th>
                    <th>Profile</th>
                    <th>Reg #</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Contact</th>
                    <th>Active</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $x=0 ?>
                @foreach ($employee as $data)
                <tr>
                    <td>{{$x+1}}</td>
                    <td><img src="{{asset('storage/'.$data->usermeta->profile)}}" height="80px" width="80px"></td>
                    <td>{{$data->usermeta->reg_number}}</td>
                    <td>{{$data->name}}</td>
                    <td class="text-right">{{ $data->email}}</td>
                    <td class="text-right">{{$data->usermeta->cell_no}}</td>
                    <td class="text-right">{{ $data->usermeta->status}}</td>
                    <td data-field="Actions" data-autohide-disabled="false" aria-label="null" class="datatable-cell">
                        <span style="overflow: visible; position: relative; width: 125px;">
                            <a href="{{route('employee.edit', $data->id)}}" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">
                                <span class="svg-icon svg-icon-md">
                                    <i class="fas fa-pen"></i>
                                </span>
                            </a>
                            <form method="Post" action="{{ route('employee.destroy',$data->id) }}" style="display: -webkit-inline-box;">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-clean btn-icon" title="Delete">
                                    <span class="svg-icon svg-icon-md">
                                        <i class="fas fa-trash"></i>
                                    </span>
                                </button>
                            </form>
                        </span>
                    </td>
                </tr>
                <?php $x++; ?>
                @endforeach
            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
@endsection
