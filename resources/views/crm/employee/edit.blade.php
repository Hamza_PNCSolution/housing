@extends('crm.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            Update Manager Profile
        </h3>
    </div>
    <!--begin::Form-->
    <form method="POST" action="{{route('employee.update', $employee->id)}}" enctype="multipart/form-data" class="form">
        @csrf
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{$employee->id}}"/>
        <div class="card-body">
            <div class="form-group row">
                <div class="col-lg-12">
                    <h6>Profile Picture: <span class="text-danger font-weight-bolder">*</span></h6>
                    <div class="image-input image-input-outline" id="kt_image_4"
                        style="background-image: url({{ asset('crm/assets/media/users/blank.png') }}) ">

                        <div class="image-input-wrapper"
                            style="background-image: url({{ asset('/storage/'.$employee->usermeta->profile) }}) "></div>

                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                            data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                            <i class="fa fa-pen icon-sm text-muted"></i>
                            <input type="file" name="profile" accept=".png, .jpg, .jpeg"/>
                            <input type="hidden" name="profile_avatar_remove" />
                        </label>

                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                            data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                        </span>

                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                            data-action="remove" data-toggle="tooltip" title="Remove avatar">
                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Reg Number:<span class="text-danger font-weight-bolder">*</span></label>
                    <input type="number" name="reg_number" class="form-control"
                        placeholder="Enter reg number" required value="{{$employee->usermeta->reg_number}}"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Name: <span class="text-danger font-weight-bolder">*</span></label>
                    <input type="text" name="name" class="form-control"
                        placeholder="Enter name" required value="{{$employee->name}}"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Father Name:<span class="text-danger font-weight-bolder">*</span></label>
                    <input type="text" name="father_name" class="form-control"
                        placeholder="Enter father name" required value="{{$employee->usermeta->father_name}}"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Date of Birth:<span class="text-danger font-weight-bolder">*</span></label>
                    <input type="date" name="date_of_birth" class="form-control"
                        placeholder="Enter father name" required value="{{$employee->usermeta->data_of_birth}}"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Salary  <span class="text-danger font-weight-bolder">*</span></label>
                    <input type="number" placeholder="Enter the salary" min="10000" max="1000000" step="0.001" name="salary" class="form-control" required="" value="{{$employee->usermeta->salary}}"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Gender: <span class="text-danger font-weight-bolder">*</span></label>
                    <select type="date" name="gender" class="form-control" required>
                        <option value="">Please Select Gender</option>
                        <option value="male" {{ ($employee->usermeta->gender == 'male') ? 'selected': null }}>Male</option>
                        <option value="female" {{ ($employee->usermeta->gender == 'female') ? 'selected': null }}>Female</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>C.N.I.C No: <span class="text-danger font-weight-bolder">*</span></label>
                    <input type="text" data-inputmask="'mask': '99999-9999999-9'" placeholder="XXXXX-XXXXXXX-X"
                        name="managercnic_no" class="form-control managercnic " required="" value="{{$employee->usermeta->cnic_no}}"/>
                </div>
            </div>
            {{-- @dd($employee) --}}
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>C.N.I.C Picture:<span class="text-danger font-weight-bolder">*</span></label>
                    <input type="file" class="dropify" data-height="200" data-allowed-file-extensions="png jpg jpeg" name="managercnic[]"
                        data-errors-position="outside" data-max-file-size-preview="2M" data-show-errors="true" multiple data-default-file="{{ asset('/storage/'.$employee->employeecnic[0]->images) }}" required/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Cell No:<span class="text-danger font-weight-bolder">*</span></label>
                    <input type="tel" name="cell" class="form-control"
                        placeholder="Enter full cell nnumber" required value="{{$employee->usermeta->cell_no}}"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Phone No:<span class="text-danger font-weight-bolder">*</span></label>
                    <input type="tel" name="phone" class="form-control"
                        placeholder="Enter full phone number" required value="{{$employee->usermeta->phone_no}}"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Email<span class="text-danger font-weight-bolder">*</span></label>
                    <input type="email" name="email" class="form-control"
                        placeholder="Enter full phone number" required value="{{$employee->email}}"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Address:<span class="text-danger font-weight-bolder">*</span></label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="address" placeholder="Enter address" value="{{$employee->usermeta->address}}" required/>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>City:<span class="text-danger font-weight-bolder">*</span></label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="city" placeholder="Enter city" value="{{$employee->usermeta->city}}" required/>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Status:<span class="text-danger font-weight-bolder">*</span></label>
                    <select type="date" name="status" class="form-control" required>
                        <option value="">Please Select Status</option>
                        <option value="yes" {{ ($employee->usermeta->status == 'yes') ? 'selected': null }} >Yes</option>
                        <option value="no" {{ ($employee->usermeta->status == 'no') ? 'selected': null }} >No</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row text-center">
                <div class="col-lg-2">
                    <button type="submit" class="btn btn-primary btn-lg btn-block btn-check">Save</button>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection
@section('footer.script')
<script>
    var avatar4 = new KTImageInput('kt_image_4');

    avatar4.on('cancel', function (imageInput) {
        swal.fire({
            title: 'Image successfully changed !',
            type: 'success',
            buttonsStyling: false,
            confirmButtonText: 'Awesome!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
    });

    avatar4.on('change', function (imageInput) {
        swal.fire({
            title: 'Image successfully changed !',
            type: 'success',
            buttonsStyling: false,
            confirmButtonText: 'Awesome!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
    });

    avatar4.on('remove', function (imageInput) {
        swal.fire({
            title: 'Image successfully removed !',
            type: 'error',
            buttonsStyling: false,
            confirmButtonText: 'Got it!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
    });

    $('.managercnic').change('change', function () {
        // var idToTest = '12345-1234567-1',
        var checkcinic = $('.managercnic').val();
        myRegExp = new RegExp(/\d{5}-\d{7}-\d/);

        if (myRegExp.test(checkcinic)) {
            $('.btn-check').removeAttr('disabled');
            $('.managercnic').removeClass('is-invalid');
        } else {
            $('.managercnic').addClass('is-invalid');
            $('.btn-check').attr('disabled', 'disabled');
        }
    });

</script>
@endsection
