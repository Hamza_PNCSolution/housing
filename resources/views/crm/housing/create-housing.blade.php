@extends('crm.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            New Housing Scheme
        </h3>
    </div>
    <div class="panel panel-default">
        @if (session('status'))
            <div class="alert alert-success">{{ session('status') }}</div>
        @endif
    </div>
    <!--begin::Form-->
    <form action="{{ route('housing.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
            <div class="form-group row">
                <label class="col-2 col-form-label">Name<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                <div class="col-10">
                    <input class="form-control" type="text" name="name" id="example-text-input" placeholder="Enter the name" required/>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-2 col-form-label">Manager<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                <div class="col-10">
                    <select class="form-control select2" name="managers[]" id="managerSelect" multiple="multiple" required>
                        @foreach ($manager as $data)
                            <option value="{{ $data->id }}">{{$data->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-2 col-form-label">Employee<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                <div class="col-10">
                    <select class="form-control select2" name="employees[]" id="employeeSelect" multiple="multiple" required>
                        @foreach ($employee as $data)
                            <option value="{{ $data->id }}">{{$data->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Phone No.<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                <div class="col-10">
                    <input class="form-control" type="tel" name="phone" id="example-text-input" placeholder="Enter the contact number" required/>
                </div>
            </div>

            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Email<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                <div class="col-10">
                    <input class="form-control" type="email" name="email" id="example-text-input" placeholder="Enter the email" required/>
                </div>
            </div>

            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Address<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                <div class="col-10">
                    <input class="form-control" type="text" name="address" id="example-text-input" placeholder="Enter the address" required/>
                </div>
            </div>

            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Website Link<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                <div class="col-10">
                    <input class="form-control" type="text" name="website" id="example-text-input" placeholder="Enter the website link" required/>
                </div>
            </div>

            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Logo<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                <div class="col-10">
                    <input type="file" class="dropify" data-height="200" data-allowed-file-extensions="png jpg jpeg" data-errors-position="outside" name="logo"  data-max-file-size-preview="2M"  data-show-errors="true" required/>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-2">
                    <button type="submit" class="btn btn-success btn-lg btn-block">Save</button>
                </div>
                <div class="col-10">
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
@section('footer.script')
<script>
    $(document).ready(function() {
            $('#managerSelect').select2({
                placeholder: "Select Managers",
                allowClear: true
            });

            $('#employeeSelect').select2({
                placeholder: "Select Employees",
                allowClear: true
            });
        });
</script>
@endsection
