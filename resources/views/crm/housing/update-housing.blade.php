@extends('crm.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            Update Housing Scheme
        </h3>
    </div>
    <!--begin::Form-->
    <form action="{{ route('housing.update', $housing->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="card-body">
            <div class="form-group row">
                <label class="col-2 col-form-label">Name<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                <div class="col-10">
                    <input class="form-control" type="hidden" name="id" id="example-text-input" value="{{$housing->id}}"/>
                    <input class="form-control" type="text" name="name" id="example-text-input" value="{{$housing->name}}" required/>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 col-form-label">Manager<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                <div class="col-10">
                    <select class="form-control select2" name="manager[]" id="managerSelect" multiple required>
                        @foreach ($manager as $key => $data)
                            @if (isset($housing->manager[$key]))
                                <option value="{{ $data->id }}" {{ ($data->id == $housing->manager[$key]->id) ? 'selected' : null  }}>{{ $data->name }}</option>
                            @else
                                <option value="{{ $data->id }}">{{ $data->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 col-form-label">Employee<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                <div class="col-10">
                    <select class="form-control select2" name="employee[]" id="employeeSelect" multiple required>
                        @foreach ($employee as $key => $data)
                            @if (isset($housing->employee[$key]))
                                <option value="{{ $data->id }}" {{ ($data->id == $housing->employee[$key]->id) ? 'selected' : null  }}>{{ $data->name }}</option>
                            @else
                                <option value="{{ $data->id }}">{{ $data->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Phone No.<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                <div class="col-10">
                    <input class="form-control" type="tel" name="phone" id="example-text-input" value="{{$housing->number}}" required/>
                </div>
            </div>
            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Email<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                <div class="col-10">
                    <input class="form-control" type="email" name="email" id="example-text-input" placeholder="Enter the email" value="{{$housing->email}}" required/>
                </div>
            </div>
            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Address<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                <div class="col-10">
                    <input class="form-control" type="address" name="address" id="example-text-input" value="{{$housing->address}}" required/>
                </div>
            </div>
            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Website Link<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                <div class="col-10">
                    <input class="form-control" type="text" name="website" id="example-text-input" placeholder="Enter the website link" value="{{$housing->website}}" required/>
                </div>
            </div>

            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Logo<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                <div class="col-10">
                    <input type="file" class="dropify" data-height="200" data-default-file="{{ asset('storage/'.optional($housing)->logo) }}" data-allowed-file-extensions="pdf png psd" data-errors-position="outside" name="logo"  data-max-file-size-preview="2M"  data-show-errors="true"/>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-2">
                    <button type="submit" class="btn btn-success btn-lg btn-block">Update</button>
                </div>
                <div class="col-10">
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
@section('footer.script')
<script>
    $(document).ready(function() {
            $('#managerSelect').select2({
                placeholder: "Select Managers",
                allowClear: true
            });

            $('#employeeSelect').select2({
                placeholder: "Select Employees",
                allowClear: true
            });
        });
</script>
@endsection
