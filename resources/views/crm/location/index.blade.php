@extends('crm.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Locations of Scheme
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            <a href="{{ route('create.location') }}" class="btn btn-primary font-weight-bolder">
                <span class="svg-icon svg-icon-md">
                    <i class="fa fa-plus-circle"></i>
                </span>New Record</a>
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..."
                                    id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--end: Search Form-->
        <!--begin: Datatable-->
        <div class="panel panel-default">
            @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @elseif (session('delete'))
                <div class="alert alert-danger">{{ session('delete') }}</div>
            @elseif (session('update'))
                <div class="alert alert-success">{{ session('update') }}</div>
            @endif
        </div>
        <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Housing Scheme</th>
                    <th>Description</th>
                    <th>Location Type</th>
                    @hasrole('admin')
                    <th>Deleted</th>
                    <th>Delete By</th>
                    @endhasrole
                    <th >Action</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($scheme as $key => $item)
                <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{ $item->scheme->name }}</td>
                    <td>{{ $item->text}}</td>
                    <td>{{ strtoupper($item->type) }}</td>
                    @hasrole('admin')
                    <td>{!! ($item->delete_at == 1) ? '<span class="badge badge-danger text-white font-weight-bolder font-size-lg">Yes</span>' : '<span class="badge badge-success text-white font-weight-bolder font-size-lg">No</span>' !!}</td>
                    <td>{!! (isset($item->users->name)) ? '<p class="text-primary font-weight-bolder font-size-lg pt-2">'.strtoupper($item->users->name).'</p>' : '<p class="text-primary font-weight-bolder font-size-lg pt-2">No User</p>' !!}</td>
                    @endhasrole
                    <td data-field="Actions" data-autohide-disabled="false" aria-label="null" class="datatable-cell">
                        <span style="overflow: visible; position: relative; width: 125px;">
                            <a href="{{ route('edit.location',$item->id) }}" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">
                                <span class="svg-icon svg-icon-md">
                                    <i class="fas fa-pen"></i>
                                </span>
                            </a>
                            <a href="{{ route('delete.location',$item->id) }}" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">
                                <span class="svg-icon svg-icon-md">
                                    <i class="fas fa-trash  "></i>
                                </span>
                            </a>
                        </span>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
@endsection
