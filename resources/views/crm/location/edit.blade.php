@extends('crm.layouts.app')
@section('content')
@if (count($scheme) == 0)
<div class="alert alert-custom alert-white alert-shadow fade show gutter-b" role="alert">
    <div class="alert-icon">
        <span class="svg-icon svg-icon-primary svg-icon-xl">
            <i class="fas fa-info text-warning"></i>
        </span>
    </div>
    <div class="alert-text"><code>Info</code> PLease create housing scheme first for location creation</div>
</div>
@endif
<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            New Scheme Location
        </h3>
    </div>
    <div class="panel panel-default">
        @if (session('status'))
            <div class="alert alert-success">{{ session('status') }}</div>
        @endif
    </div>
    <!--begin::Form-->
    <form action="{{ route('update.location') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
            <div class="form-group row">
                @hasrole('admin')
                <label class="col-2 col-form-label">Housing Scheme<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <div class="col-10">
                        <select class="form-control" name="scheme_id" id="example-text-input" required>
                            <option value="">Please Select Housing Scheme</option>
                            @foreach ($scheme as $data)
                                <option value="{{ $data->id }}" {{ ($data->id == $location->scheme_id) ? 'selected' : null }}>{{$data->name}}</option>
                            @endforeach
                        </select>
                    </div>
                @else
                <label class="col-2 col-form-label">Housing Scheme<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <div class="col-10">
                        <input class="form-control" type="text" name="scheme_name" value="{{ $location->scheme->name ?? '' }}"  readonly/>
                        <input class="form-control" type="hidden" name="scheme_id" value="{{ $location->scheme_id ?? '' }}"/>
                    </div>
                @endhasrole
            </div>

            <div class="form-group row">
                <label class="col-2 col-form-label">Description<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                <div class="col-10">
                    <input class="form-control" type="hidden" name="id" value="{{ $location->id ?? '' }}"/>
                    <input class="form-control" type="text" name="description" id="example-text-input" value="{{ $location->text ?? '' }}" placeholder="Enter the description of location" required/>
                </div>
            </div>

            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Type<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                <div class="col-10">
                    <select class="form-control" name="type" id="example-text-input" required>
                        <option value="">Please Select Location Type</option>
                        <option value="phase" {{ ($location->type == 'phase') ? 'selected' : null }}>Phase</option>
                        <option value="block" {{ ($location->type == 'block') ? 'selected' : null }}>Block</option>
                        <option value="location" {{ ($location->type == 'location') ? 'selected' : null }}>Location</option>
                    </select>
                </div>
            </div>

        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-2">
                    <button type="submit" class="btn btn-success btn-lg btn-block">Update</button>
                </div>
                <div class="col-10">
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
