@extends('crm.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">All Sales</h3>
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..."
                                    id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--begin: Datatable-->
        <div class="panel panel-default">
            @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @elseif (session('delete'))
                <div class="alert alert-danger">{{ session('delete') }}</div>
            @elseif (session('update'))
                <div class="alert alert-success">{{ session('update') }}</div>
            @endif
        </div>
        <br>
        <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
            <thead>
                <tr>
                    <th >Sr No</th>
                    <th>Plot Id</th>
                    <th>Plot Type</th>
                    <th>Client Name</th>
                    <th>Phone No</th>
                    <th>Amount</th>
                    <th>Pending Amount</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($sale as $key => $data)
                <tr>
                    <td >{{ ++$key }}</td>
                    <td>{{$data->file_no}}</td>
                    <td>{{$data->client[0]->name }}</td>
                    <td >{{$data->plot[0]->plot_id }}</td>
                    <td >Rs. {{number_format($data->plot[0]->totalcost)}}</td>
                    <td >{{$data->type }}</td>
                    <td>
                        <span style="width: 122px;">
                            <a href="{{ route('view.sale',$data->id) }}" class="btn btn-sm btn-clean btn-icon" title="View details">
                                <span class="svg-icon svg-icon-md">
                                    <i class="fas fa-eye"></i>
                                </span>
                            </a>
                            <a href="{{ route('installment.pay',$data->id) }}" class="btn btn-sm btn-clean btn-icon" title="Installment">
                                <span class="svg-icon svg-icon-md">
                                    <i class="fas fa-money-bill"></i>
                                </span>
                            </a>
                            <a href="{{route('sale.delete',$data->id)}}" class="btn btn-sm btn-clean btn-icon" title="Sale Delete">
                                <span class="svg-icon svg-icon-md">
                                    <i class="fas fa-trash"></i>
                                </span>
                            </a>
                        </span>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
@endsection
{{-- @section('footer.script')
<script src="{{ asset('crm/assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>
<script src="{{ asset('crm/assets/js/pages/crud/datatables/basic/scrollable.js')}}"></script>
@endsection --}}
