@extends('crm.layouts.app')
@section('content')
<div class="card card-custom overflow-hidden">
    <div class="card-body p-0">
        <!-- begin: Invoice-->
        <!-- begin: Invoice header-->
        <div class="row justify-content-center py-8 px-8 py-md-12 px-md-0">
            <div class="col-md-10">
                <h1 class="display-3 font-weight-bolder pb-5 text-center">Sale Agreement</h1>
                <div class="d-flex justify-content-between pb-10 pb-md-10 flex-column flex-md-row">
                    <h1 class="display-3 font-weight-400 mb-10">{{$sale->plot[0]->housing->name}}</h1>
                    <div class="d-flex flex-column align-items-md-end px-0">
                        <!--begin::Logo-->
                        <a href="#" class="mb-5">
                            <img src="{{ asset('storage/'.$sale->plot[0]->housing->logo) }}" alt="Hosuing Logo" height="96px"
                                width="96px" />
                        </a>
                        <!--end::Logo-->
                        <span class="d-flex flex-column align-items-md-end opacity-70">
                            <span>{{$sale->plot[0]->housing->address}}</span>
                            <span>{{$sale->plot[0]->housing->number}}</span>
                        </span>
                    </div>
                </div>
                <div class="border-bottom w-100"></div>
                <div class="d-flex justify-content-between pt-6">
                    <div class="d-flex flex-column flex-root">
                        <span class="font-weight-bolder mb-2">Agreement Date</span>
                        <span class="opacity-70">{{$sale->created_at->format('Y-m-d')}}</span>
                    </div>
                    <div class="d-flex flex-column flex-root">
                        <span class="font-weight-bolder mb-2">Sale TYPE</span>
                        <span class="text-primary font-weight-bolder opacity-70">{{$sale->type}}</span>
                    </div>
                    <div class="d-flex flex-column flex-root">
                        <span class="font-weight-bolder mb-2">Advance</span>
                        <span class="text-primary font-weight-bolder opacity-70">Rs {{number_format($sale->advance)}}</span>
                    </div>
                    <div class="d-flex flex-column flex-root">
                        <span class="font-weight-bolder mb-2">Remaning Amount</span>
                        <span class="text-primary font-weight-bolder opacity-70">Rs {{number_format($sale->remaning_amount)}}</span>
                    </div>
                </div>
                <div class="d-flex justify-content-between pt-6">
                    <div class="d-flex flex-column flex-root">
                        <span class="font-weight-bolder mb-2">Client Name</span>
                        <span class="opacity-70">{{$sale->client[0]->name}}</span>
                    </div>
                    <div class="d-flex flex-column flex-root">
                        <span class="font-weight-bolder mb-2">Nominee</span>
                        <span class="opacity-70">{{$sale->client[0]->nominee->name}}</span>
                    </div>
                    <div class="d-flex flex-column flex-root">
                        <span class="font-weight-bolder mb-2">Relationship</span>
                        <span class="opacity-70">{{$sale->client[0]->nominee->relationship}}</span>
                    </div>
                </div>

            </div>
        </div>
        <!-- end: Invoice header-->
        <!-- begin: Invoice footer-->
        <div class="row justify-content-center bg-gray-100 py-8 px-8 py-md-10 px-md-0">
            <div class="col-md-10">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="font-weight-bold text-muted text-uppercase">Plot ID</th>
                                <th class="font-weight-bold text-muted text-uppercase">MARLA</th>
                                <th class="font-weight-bold text-muted text-uppercase">COST PER MARLA</th>
                                <th class="font-weight-bold text-muted text-uppercase">TOTAL COST</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="font-weight-bolder">
                                <td>{{$sale->plot[0]->plot_id}}</td>
                                <td>{{$sale->plot[0]->marla}}</td>
                                <td>{{number_format($sale->plot[0]->cost,2)}}</td>
                                <td class="text-danger font-size-h3 font-weight-boldest">Rs {{number_format($sale->plot[0]->totalcost)}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- end: Invoice footer-->
        <!-- begin: Invoice body-->

        <div class="row justify-content-center py-8 px-8 py-md-8 px-md-0">
            <div class="col-md-10">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="pl-0 font-weight-bold text-muted text-uppercase">Size Description</th>
                                <th class="text-right font-weight-bold text-muted text-uppercase">Width</th>
                                <th class="text-right font-weight-bold text-muted text-uppercase">Length</th>
                                <th class="text-right pr-0 font-weight-bold text-muted text-uppercase">Sq Ft</th>
                                <th class="text-right pr-0 font-weight-bold text-muted text-uppercase">Sq Mt</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr class="font-weight-boldest">
                                <td class="pl-0 pt-5">Size</td>
                                <td class="text-right pt-5">{{$sale->plot[0]->width}}</td>
                                <td class="text-right pt-5">{{$sale->plot[0]->length}}</td>
                                <td class="text-danger pr-0 pt-5 text-right">{{$sale->plot[0]->sqrft}}</td>
                                <td class="text-danger pr-0 pt-5 text-right">{{$sale->plot[0]->sqrmt}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row justify-content-center py-4 px-4 px-md-0">
            <div class="col-md-10">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="pl-0 font-weight-bold text-muted text-uppercase">No. of Installment</th>
                                <th class="text-right font-weight-bold text-muted text-uppercase">Amount</th>
                                <th class="text-right font-weight-bold text-muted text-uppercase">Last Date</th>
                                <th class="text-right pr-0 font-weight-bold text-muted text-uppercase">Status</th>

                            </tr>
                        </thead>
                        <tbody>
                            @if($sale->type == 'On Cash')
                                @foreach ($sale->oncash as $key=> $oncash)
                                <tr class="font-weight-boldest">
                                    <td class="pl-0 pt-5">{{++$key}}</td>
                                    <td class="text-right pt-5">Rs. {{number_format($oncash->installment)}}</td>
                                    <td class="text-right pt-5">{{$oncash->date}}</td>
                                    <td class="@if($oncash->paid == 0) text-danger @else text-success @endif  pr-0 pt-5 text-right">{{($oncash->paid == 0) ? 'UnPaid' : 'Paid' }}</td>
                                </tr>
                                @endforeach
                            @else
                                @foreach ($sale->installment as $key=> $installment)
                                <tr class="font-weight-boldest">
                                    <td class="pl-0 pt-5">{{++$key}}</td>
                                    <td class="text-right pt-5">Rs. {{number_format($installment->installment)}}</td>
                                    <td class="text-right pt-5">{{$installment->date}}</td>
                                    <td class="@if($installment->paid == 0) text-danger @else text-success @endif  pr-0 pt-5 text-right">{{($installment->paid == 0) ? 'UnPaid' : 'Paid' }}</td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- end: Invoice body-->
        <!-- begin: Invoice action-->
        <div class="row justify-content-center py-8 px-8 py-md-10 px-md-0">
            <div class="col-md-10">
                <div class="d-flex justify-content-between">
                    <a href="#" type="button" class="btn btn-primary font-weight-bold" id="btnprint"
                        onclick="print_page()">Print Invoice</a>
                </div>
            </div>
        </div>
        <!-- end: Invoice action-->
        <!-- end: Invoice-->
    </div>
</div>
@endsection
@section('footer.script')
<script>
    function print_page() {
        var ButtonControl = document.getElementById("btnprint");
        ButtonControl.style.visibility = "hidden";
        window.print();
    }
</script>
@endsection
