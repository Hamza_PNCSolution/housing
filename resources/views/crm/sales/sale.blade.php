@extends('crm.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            New Sales Agreement
        </h3>
    </div>
    <!--begin::Form-->
    <form method="POST" action="{{route('save.sale')}}" enctype="multipart/form-data" class="form">
        @csrf
        <div class="card-body">
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>File No</label>
                    <input type="text" name="file_no" class="form-control" required/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Name of Applicant</label>
                    <select name="customer_id" class="form-control customer" required>
                        <option value="">Please Select Applicant</option>
                        @foreach ($customer as $data )
                            <option value="{{$data->id}}" data-id="{{$data->id}}">{{$data->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-lg-6">
                    <label>S/D/W of</label>
                    <input type="text" name="father_name" class="form-control" readonly/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>C.N.I.C No</label>
                    <input type="text" name="cnic_no" data-inputmask="'mask': '99999-9999999-9'" placeholder="XXXXX-XXXXXXX-X"
                    name="customercnic" class="form-control customercnic" readonly/>
                </div>
                <div class="col-lg-6">
                    <label>Contact No</label>
                    <input type="tel" name="cell_no" class="form-control"  readonly/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Address</label>
                    <input type="text" name="address" class="form-control"  readonly/>
                </div>
                <div class="col-lg-6">
                    <label>City</label>
                    <input type="text" name="city" class="form-control" readonly/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>C.N.I.C Picture:</label>
                    <input type="file" class="customerecnic" data-height="200" data-allowed-file-extensions="pdf png psd" name="custumorcnic[]"
                        data-errors-position="outside" data-max-file-size-preview="2M" data-show-errors="true"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Name of Nominee(s)</label>
                    <input type="text" name="name" class="form-control" placeholder="Enter Nominee name" readonly/>
                </div>
                <div class="col-lg-6">
                    <label>Father of Nominee(s)</label>
                    <input type="text" name="father_name" class="form-control"  readonly/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>C.N.I.C No</label>
                    <input type="text" name="cnic_no" data-inputmask="'mask': '99999-9999999-9'" placeholder="XXXXX-XXXXXXX-X"
                    name="nomineecnic" class="form-control " readonly/>
                </div>
                <div class="col-lg-6">
                    <label>Relationship</label>
                    <input type="text" name="relationship" class="form-control" readonly/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>C.N.I.C Picture:</label>
                    <input type="file" class="nomineecnic" data-height="200" data-allowed-file-extensions="pdf png psd" name="nomineecnic[]"
                        data-errors-position="outside" data-max-file-size-preview="2M" data-show-errors="true" />
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Agent Name</label>
                    <div class="input-group">
                        <select class="form-control agent" name="agent_id" required>
                                <option value="">Please Select Agent</option>
                            @foreach ($agent as $data)
                                <option value="{{$data->id}}" data-id="{{$data->id}}">{{$data->name}}</option>
                            @endforeach
                        </option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label>Agent Comission</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="comission"  readonly/>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Plot No</label>
                    <div class="input-group">
                        <select class="form-control plot" name="plot_no" required>
                            <option value="">Please Select Plot No</option>
                            @foreach ($plot as $data)
                                <option value="{{$data->id}}" data-id="{{$data->id}}">{{$data->plot_id}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label>Plot Type</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="type"  readonly/>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Rate Per-Marla</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="cost"  readonly/>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label>Total Marla</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="marla"  readonly/>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Size</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="sqrmt"  readonly/>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label>Total Cost of Plot</label>
                    <div class="input-group">
                        <input type="text" class="form-control totalcost" name="totalcost"  readonly/>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    <label>Advance</label>
                    <div class="input-group">
                        <input type="number" class="form-control advance" name="advance"  required/>
                    </div>
                </div>
                <div class="col-lg-4">
                    <label>Remaning Amount</label>
                    <div class="input-group">
                        <input type="number" class="form-control remaning_amount" name="remaning_amount"  readonly/>
                    </div>
                </div>
                <div class="col-lg-4">
                    <label>Sale Type</label>
                    <select class="form-control" name="sale_type" id="sale_type"x required>
                        <option value="">Please Select Sale Type</option>
                        <option value="On Cash">On Cash</option>
                        <option value="Isntallment">Isntallment</option>
                    </select>
                </div>
            </div>

            <div id="on_cash" style="display: none;">
                <div class="form-group row">
                    <div class="col-lg-4">
                        <label>No. of Installment</label>
                        <div class="input-group">
                            <input type="number" class="form-control" name="installment[]" value="1"  readonly/>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <label>Amount To Be Paid</label>
                        <div class="input-group">
                            <input type="number" class="form-control" name="inst_amount[]" id="inst1_amount"  readonly/>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <label>Last Date</label>
                        <div class="input-group">
                            <input type="date" class="form-control" name="inst_date[]" id="inst1_date"  readonly/>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-4">
                        <label>No. of Installment</label>
                        <div class="input-group">
                            <input type="number" class="form-control" name="installment[]" value="2"  readonly/>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <label>Amount To Be Paid</label>
                        <div class="input-group">
                            <input type="number" class="form-control" name="inst_amount[]" id="inst2_amount"  readonly/>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <label>Last Date</label>
                        <div class="input-group">
                            <input type="date" class="form-control" name="inst_date[]" id="inst2_date"  readonly/>
                        </div>
                    </div>
                </div>
            </div>

            <div id="on_installment" style="display:none;">
                <div class="form-group row">
                    <div class="col-lg-4">
                        <label>Isntallment Type</label>
                        <select class="form-control" name="Isntallment_type" id="Isntallment_type" required>
                            <option value="" disselected>Please Select Isntallment Type</option>
                            <option value="monthly">Monthly</option>
                            <option value="quarterly">Quarterly</option>
                            <option value="half-yearly">Half Yearly</option>
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <label>No. of Installment</label>
                        <div class="input-group">
                            <input type="number" class="form-control" name="no_installment" id="no_installment" required/>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <label>Starting Date</label>
                        <div class="input-group">
                            <input type="date" class="form-control" name="starting_date" id="starting_date"  required/>
                        </div>
                    </div>
                </div>
                {{-- for list of installment --}}
                <div class="form-group row" id="list_installment" style="display: none;">
                    <div class="col-lg-4">
                        <label>No. of Installment</label>
                        <div id="no_installments"></div>
                    </div>
                    <div class="col-lg-4">
                        <label>Installment Amount</label>
                        <div id="amount_installment"></div>
                    </div>
                    <div class="col-lg-4">
                        <label>Last Date of Installment</label>
                        <div id="date_installment"></div>
                    </div>
                </div>
            </div>

        </div>
        <div class="card-footer">
            <div class="row text-center">
                <div class="col-lg-2">
                    <button type="submit" class="btn btn-primary btn-lg btn-block btn-check">Save</button>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection
@section('footer.script')
<script src="{{ asset('crm/assets/js/sales.js')}}"></script>
<script>

</script>
@endsection
