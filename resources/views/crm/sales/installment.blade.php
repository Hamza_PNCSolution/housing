@extends('crm.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Installment</h3>
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..."
                                    id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--begin: Datatable-->
        <div class="panel panel-default">
            @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @elseif (session('delete'))
                <div class="alert alert-danger">{{ session('delete') }}</div>
            @elseif (session('update'))
                <div class="alert alert-success">{{ session('update') }}</div>
            @endif
        </div>
        <br>
        <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
            <thead>
                <tr>
                    <th>Sr No</th>
                    <th>Amount</th>
                    <th>Date</th>
                    <th>Payment Status</th>
                </tr>
            </thead>
            <tbody>
                @if($sale->type == 'On Cash')
                    @foreach ($sale->oncash as $key=> $oncash)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>Rs. {{number_format($oncash->installment)}}</td>
                        <td>{{$oncash->date}}</td>
                        <td class="@if($oncash->paid == 0) text-danger @else text-success @endif">
                            @if($oncash->paid == 0)
                                <form method="POST" action="{{route('installment.cash',$oncash->id)}}">
                                    @csrf
                                    <button type="submit" class="btn btn-danger">UNPAID</button>
                                </form>
                            @else
                            <form method="POST" action="{{route('uninstallment.cash',$oncash->id)}}">
                                @csrf
                                <button type="submit" class="btn btn-success">PAID</button>
                            </form>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                @else
                    @foreach ($sale->installment as $key=> $installment)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>Rs. {{number_format($installment->installment)}}</td>
                        <td>{{$installment->date}}</td>
                        <td class="@if($installment->paid == 0) text-danger @else text-success @endif">
                            @if($installment->paid == 0)
                            <form method="POST" action="{{route('installment.monthly',$installment->id)}}">
                                @csrf
                                <button type="submit" class="btn btn-danger">UNPAID</button>
                            </form>
                        @else
                        <form method="POST" action="{{route('uninstallment.monthly',$installment->id)}}">
                            @csrf
                            <button type="submit" class="btn btn-success">PAID</button>
                        </form>
                        @endif
                        </td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
@endsection
@section('footer.script')
<script>

</script>
@endsection
