@extends('crm.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            New Client
        </h3>
        {{-- @dd(auth()->user()->employee->scheme_id) --}}
    </div>
    <!--begin::Form-->
    <form method="POST" action="{{route('client.store')}}" enctype="multipart/form-data" class="form">
        @csrf
        <div class="card-body">
            <div class="form-group row">
                <div class="col-lg-6">
                    <h3 class="card-title">Customer Information</h3>
                </div>
                <div class="col-lg-6">
                    <h3 class="card-title">Nominee Information</h3>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <h6>Customer Picture:<span class="text-danger font-weight-bolder font-size-lg">*</span></h6>
                    <div class="image-input image-input-outline" id="kt_image_4"
                        style="background-image: url({{ asset('crm/assets/media/users/blank.png') }}) ">

                        <div class="image-input-wrapper"
                            style="background-image: url({{ asset('crm/assets/media/users/100_1.jpg') }}) "></div>

                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                            data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                            <i class="fa fa-pen icon-sm text-muted"></i>
                            <input type="file" name="profile_customer" accept=".png, .jpg, .jpeg" required/>
                            <input type="hidden" name="profile_avatar_remove" />
                        </label>

                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                            data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                        </span>

                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                            data-action="remove" data-toggle="tooltip" title="Remove avatar">
                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                        </span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <h6>Nominee Picture:<span class="text-danger font-weight-bolder font-size-lg">*</span></h6>
                    <div class="image-input image-input-outline" id="kt_image_5"
                        style="background-image: url({{ asset('crm/assets/media/users/blank.png') }}) ">
                        <div class="image-input-wrapper"
                            style="background-image: url({{ asset('crm/assets/media/users/100_1.jpg')  }})"></div>

                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                            data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                            <i class="fa fa-pen icon-sm text-muted"></i>
                            <input type="file" name="profile_nominee" accept=".png, .jpg, .jpeg" required/>
                            <input type="hidden" name="profile_avatar_remove" />
                        </label>

                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                            data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                        </span>

                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                            data-action="remove" data-toggle="tooltip" title="Remove avatar">
                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Name:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="text" name="customername" class="form-control"
                        placeholder="Enter full customer name" required/>
                </div>
                <div class="col-lg-6">
                    <label>Name:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="text" name="nomineename" class="form-control" placeholder="Enter full nominee name" required/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Father / Husband Name:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="text" name="customerfahtername" class="form-control"
                        placeholder="Enter full customer father / husband name" required/>
                </div>
                <div class="col-lg-6">
                    <label>Father / Husband Name:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="text" name="nomineefathername" class="form-control"
                        placeholder="Enter full nominee father / husband name" required/>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Gender<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <select name="customergender" class="form-control" required>
                        <option value="">Please Select Gender</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                </div>
                <div class="col-lg-6">
                    <label>Gender<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <select name="nomineegender" class="form-control" required>
                        <option value="">Please Select Gender</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Date of Birth:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="date" name="customerdob" class="form-control"
                        placeholder="Enter the data of birth" required/>
                </div>
                <div class="col-lg-6">
                    <label>Date of Birth:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="date" name="nomineedob" class="form-control"
                        placeholder="Enter the data of birth" required/>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-6">
                    <label>C.N.I.C No:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="text" data-inputmask="'mask': '99999-9999999-9'" placeholder="XXXXX-XXXXXXX-X"
                        name="customercnic" class="form-control customercnic " required/>
                </div>
                <div class="col-lg-6">
                    <label>C.N.I.C No:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="text" name="nomineecnicno" class="form-control nomineecnic" placeholder="XXXXX-XXXXXXX-X"
                        required />
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>C.N.I.C Picture:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="file" class="dropify" data-height="200" data-allowed-file-extensions="png jpg jpeg" name="custumorcnic[]"
                        data-errors-position="outside" data-max-file-size-preview="2M" data-show-errors="true" multiple required/>
                </div>
                <div class="col-lg-6">
                    <label>C.N.I.C Picture:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="file" class="dropify" data-height="200" data-allowed-file-extensions="png jpg jpeg" name="nomineecnic[]"
                        data-errors-position="outside" data-max-file-size-preview="2M" data-show-errors="true" multiple required/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Cell No:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="tel" name="customercell" class="form-control"
                        placeholder="Enter full customer cell nnumber" required />
                </div>
                <div class="col-lg-6">
                    <label>Cell No:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="tel" name="nomineecell" class="form-control"
                        placeholder="Enter full nominee cell number" required/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Phone No:</label>
                    <input type="tel" name="customerphone" class="form-control"
                        placeholder="Enter full customer phone number" />
                </div>
                <div class="col-lg-6">
                    <label>Phone No:</label>
                    <input type="tel" name="nomineephone" class="form-control"
                        placeholder="Enter full nominee phone number" />
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Email</label>
                    <input type="email" name="customeremail" class="form-control"
                        placeholder="Enter full customer phone number" />
                </div>
                <div class="col-lg-6">
                    <label>Relationship<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="text" name="relationship" class="form-control"
                        placeholder="Enter the relationship" required/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Address:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="address" placeholder="Enter customer address" required/>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>City:</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="city" placeholder="Enter customer city" />
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row text-center">
                <div class="col-lg-2">
                    <button type="submit" class="btn btn-primary btn-lg btn-block btn-check">Save</button>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection
@section('footer.script')
<script>
    var avatar4 = new KTImageInput('kt_image_4');

    avatar4.on('cancel', function (imageInput) {
        swal.fire({
            title: 'Image successfully changed !',
            type: 'success',
            buttonsStyling: false,
            confirmButtonText: 'Awesome!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
    });

    avatar4.on('change', function (imageInput) {
        swal.fire({
            title: 'Image successfully changed !',
            type: 'success',
            buttonsStyling: false,
            confirmButtonText: 'Awesome!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
    });

    avatar4.on('remove', function (imageInput) {
        swal.fire({
            title: 'Image successfully removed !',
            type: 'error',
            buttonsStyling: false,
            confirmButtonText: 'Got it!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
    });

    var avatar5 = new KTImageInput('kt_image_5');

    avatar5.on('cancel', function (imageInput) {
        swal.fire({
            title: 'Image successfully changed !',
            type: 'success',
            buttonsStyling: false,
            confirmButtonText: 'Awesome!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
    });

    avatar5.on('change', function (imageInput) {
        swal.fire({
            title: 'Image successfully changed !',
            type: 'success',
            buttonsStyling: false,
            confirmButtonText: 'Awesome!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
    });

    avatar5.on('remove', function (imageInput) {
        swal.fire({
            title: 'Image successfully removed !',
            type: 'error',
            buttonsStyling: false,
            confirmButtonText: 'Got it!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
    });

    $('.customercnic , .nomineecnic ').change('change', function () {
        // var idToTest = '12345-1234567-1',
        var checkcinic = $('.customercnic').val();
        var checkcinictwo = $('.nomineecnic').val();
        myRegExp = new RegExp(/\d{5}-\d{7}-\d/);

        if (myRegExp.test(checkcinic)) {
            $('.btn-check').removeAttr('disabled');
            $('.customercnic').removeClass('is-invalid');
        } else {
            $('.customercnic').addClass('is-invalid');
            $('.btn-check').attr('disabled', 'disabled');
        } if (myRegExp.test(checkcinictwo)) {
            $('.btn-check').removeAttr('disabled');
            $('.nomineecnic').removeClass('is-invalid');
        }
        else {
            $('.nomineecnic').addClass('is-invalid');
            $('.btn-check').attr('disabled', 'disabled');
        }
    });

</script>
@endsection
