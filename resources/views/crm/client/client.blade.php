@extends('crm.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Client Detials
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            @if (auth()->user()->hasrole('admin'))
                <a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#housingscheme">
                    <span class="svg-icon svg-icon-md">
                        <i class="fa fa-home"></i>
                    </span>Scheme
                </a>
            @endif
            &nbsp;&nbsp;
            <a href="{{ route('client.create') }}" class="btn btn-primary font-weight-bolder">
                <span class="svg-icon svg-icon-md">
                    <i class="fa fa-plus-circle"></i>
                </span>Client
            </a>
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..."
                                    id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--end: Search Form-->
        <!--begin: Datatable-->
        <div class="panel panel-default">
            @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @elseif (session('delete'))
                <div class="alert alert-danger">{{ session('delete') }}</div>
            @elseif (session('update'))
                <div class="alert alert-success">{{ session('update') }}</div>
            @endif
        </div>
        <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
            <thead>
                <tr>
                    <th >#</th>
                    <th>Profile</th>
                    <th>Name</th>
                    <th>S/o</th>
                    <th>Contact</th>
                    <th>Nominee</th>
                    <th>Address</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $x=0 ?>
                @foreach ($client as $data)
                <tr>
                    <td>{{$x+1}}</td>
                    <td><img src="{{ asset('storage/'.optional($data)->profile) }}" height="80px" width="80px" /></td>
                    <td>{{$data->name}}</td>
                    <td>{{ $data->father_name }}</td>
                    <td>{{$data->cell_no}}</td>
                    <td>{{$data->nominee->name}}</td>
                    <td>{{ $data->address}}</td>
                    <td data-field="Actions" data-autohide-disabled="false" aria-label="null" class="datatable-cell">
                        <span style="overflow: visible; position: relative; width: 125px;">
                            <a href="{{route('client.edit', $data->id)}}" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">
                                <span class="svg-icon svg-icon-md">
                                    <i class="fas fa-pen"></i>
                                </span>
                            </a>
                            <form method="Post" action="{{ route('client.destroy',$data->id) }}" style="display: -webkit-inline-box;">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-clean btn-icon" title="Delete">
                                    <span class="svg-icon svg-icon-md">
                                        <i class="fas fa-trash"></i>
                                    </span>
                                </button>
                            </form>
                        </span>
                    </td>
                </tr>
                <?php $x++; ?>
                @endforeach
            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="housingscheme" tabindex="-1" role="dialog" aria-labelledby="housingschemeTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitl  e">Select Housing Scheme</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form action="{{ route('store.scheme') }}" method="POST">
            @csrf
                <div class="form-group row">
                    <label class="col-4 col-form-label">Housing Scheme<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <div class="col-8">
                        <select class="form-control" name="scheme" style="width: 100%" required>
                            <option value="">Please Select the Housing Scheme</option>
                            @foreach ($housing as $data)
                            <option value="{{ $data->id }}">{{$data->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Customers<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <div class="col-8">
                        <select class="form-control select2" name="clients[]" id="customer" multiple="multiple" required style="width: 100%">
                            @foreach ($client as $data)
                                <option value="{{ $data->id }}">{{$data->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
@endsection
@section('footer.script')
<script>
    $(document).ready(function() {
            $('#customer').select2({
                placeholder: "Select Customer",
            });
        });
</script>
@endsection
