@extends('crm.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            Update Client
        </h3>
    </div>
    <!--begin::Form-->
    <form class="form" action="{{ route('client.update', $client->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="card-body">
            <div class="form-group row">
                <div class="col-lg-6">
                    <input type="hidden" name="id" value="{{$client->id}}"/>
                    <input type="hidden" name="nominee_id" value="{{$client->nominee->id}}"/>

                    <h3 class="card-title">Customer Information</h3>
                </div>
                <div class="col-lg-6">
                    <h3 class="card-title">Nominee Information</h3>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <h6>Customer Picture:<span class="text-danger font-weight-bolder font-size-lg">*</span></h6>
                    <div class="image-input image-input-outline" id="kt_image_4"
                        style="background-image: url({{ asset('crm/assets/media/users/blank.png') }}) ">

                        <div class="image-input-wrapper"
                            style="background-image: url({{ asset('/storage/'.$client->profile) }}) "></div>

                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                            data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                            <i class="fa fa-pen icon-sm text-muted"></i>
                            <input type="file" name="profile_customer" accept=".png, .jpg, .jpeg" />
                            <input type="hidden" name="profile_avatar_remove" />
                        </label>

                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                            data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                        </span>

                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                            data-action="remove" data-toggle="tooltip" title="Remove avatar">
                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                        </span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <h6>Nominee Picture:<span class="text-danger font-weight-bolder font-size-lg">*</span></h6>
                    <div class="image-input image-input-outline" id="kt_image_5"
                        style="background-image: url({{ asset('crm/assets/media/users/blank.png') }}) ">
                        <div class="image-input-wrapper"
                            style="background-image: url({{ asset('/storage/'.$client->nominee->profile) }})"></div>

                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                            data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                            <i class="fa fa-pen icon-sm text-muted"></i>
                            <input type="file" name="profile_nominee" accept=".png, .jpg, .jpeg" />
                            <input type="hidden" name="profile_avatar_remove" />
                        </label>

                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                            data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                        </span>

                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                            data-action="remove" data-toggle="tooltip" title="Remove avatar">
                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Name:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="text" name="customername" class="form-control"
                        placeholder="Enter full customer name" value="{{$client->name}}" required/>
                </div>
                <div class="col-lg-6">
                    <label>Name:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="text" name="nomineename" class="form-control" placeholder="Enter full nominee name" value="{{$client->nominee->name}}" required/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Father / Husband Name:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="text" name="customerfahtername" class="form-control"
                        placeholder="Enter full customer father / husband name" value="{{$client->father_name}}" required/>
                </div>
                <div class="col-lg-6">
                    <label>Father / Husband Name:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="text" name="nomineefathername" class="form-control"
                        placeholder="Enter full nominee father / husband name" value="{{$client->nominee->father_name}}"  required/>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Gender<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <select name="customergender" class="form-control" required>
                        <option value="">Please Select Gender</option>
                        <option value="male" {{ ($client->gender == 'male') ? 'selected' : null }}>Male</option>
                        <option value="female" {{ ($client->gender == 'female') ? 'selected' : null }}>Female</option>
                    </select>
                </div>
                <div class="col-lg-6">
                    <label>Gender<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <select name="nomineegender" class="form-control" required>
                        <option value="">Please Select Gender</option>
                        <option value="male" {{ ($client->nominee->gender == 'male') ? 'selected' : null }}>Male</option>
                        <option value="female" {{ ($client->nominee->gender == 'female') ? 'selected' : null }}>Female</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Date of Birth:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="date" name="customerdob" class="form-control"
                        placeholder="Enter the data of birth" value="{{$client->dob}}" required/>
                </div>
                <div class="col-lg-6">
                    <label>Date of Birth:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="date" name="nomineedob" class="form-control"
                        placeholder="Enter the data of birth" value="{{$client->nominee->dob}}" required/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>C.N.I.C No:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="text" data-inputmask="'mask': '99999-9999999-9'" placeholder="XXXXX-XXXXXXX-X"
                        name="customercnic" class="form-control customercnic " required value="{{$client->cnic_no}}"/>
                </div>
                <div class="col-lg-6">
                    <label>C.N.I.C No:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="text" name="nomineecnicno" class="form-control nomineecnic" placeholder="XXXXX-XXXXXXX-X"
                        required value="{{$client->nominee->cnic_no}}"/>
                </div>
            </div>
            {{-- @dd(count($client->nominee->cnic)) --}}
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>C.N.I.C Picture:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="file" class="dropify" data-height="200" data-allowed-file-extensions="png jpg jpeg" name="custumorcnic[]" data-errors-position="outside"  data-show-errors="true" data-default-file="{{ asset('/storage/'.$client->cnic[0]->images)}}" multiple required/>
                </div>

                <div class="col-lg-6">
                    <label>C.N.I.C Picture:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="file" class="dropify" data-height="200" data-allowed-file-extensions="png jpg jpeg" name="nomineecnic[]"
                        data-errors-position="outside" data-show-errors="true"  data-default-file="{{  asset('/storage/'.$client->nominee->cnic[0]->images)}}" multiple required/>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Cell No:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="tel" name="customercell" class="form-control"
                        placeholder="Enter full customer cell nnumber" required value="{{$client->cell_no}}"/>
                </div>
                <div class="col-lg-6">
                    <label>Cell No:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="tel" name="nomineecell" class="form-control"
                        placeholder="Enter full nominee cell number" required value="{{$client->nominee->cell_no}}"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Phone No:</label>
                    <input type="tel" name="customerphone" class="form-control"
                        placeholder="Enter full customer phone number" value="{{$client->phone_no}}" />
                </div>
                <div class="col-lg-6">
                    <label>Phone No:</label>
                    <input type="tel" name="nomineephone" class="form-control"
                        placeholder="Enter full nominee phone number" value="{{$client->nominee->phone_no}}"  />
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Email</label>
                    <input type="email" name="customeremail" class="form-control"
                        placeholder="Enter full customer phone number" value="{{$client->email}}"/>
                </div>
                <div class="col-lg-6">
                    <label>Relationship<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <input type="text" name="relationship" class="form-control"
                        placeholder="Enter full nominee phone number" required value="{{$client->nominee->relationship}}"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Address:<span class="text-danger font-weight-bolder font-size-lg">*</span></label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="address" placeholder="Enter customer address" value="{{$client->address}}" required/>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>City:</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="city" placeholder="Enter customer city" value="{{$client->city}}"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row text-center">
                <div class="col-lg-2">
                    <button type="submit" class="btn btn-primary btn-lg btn-block btn-check">Save</button>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection
@section('footer.script')
<script>
    var avatar4 = new KTImageInput('kt_image_4');

    avatar4.on('cancel', function (imageInput) {
        swal.fire({
            title: 'Image successfully changed !',
            type: 'success',
            buttonsStyling: false,
            confirmButtonText: 'Awesome!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
    });

    avatar4.on('change', function (imageInput) {
        swal.fire({
            title: 'Image successfully changed !',
            type: 'success',
            buttonsStyling: false,
            confirmButtonText: 'Awesome!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
    });

    avatar4.on('remove', function (imageInput) {
        swal.fire({
            title: 'Image successfully removed !',
            type: 'error',
            buttonsStyling: false,
            confirmButtonText: 'Got it!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
    });

    var avatar5 = new KTImageInput('kt_image_5');

    avatar5.on('cancel', function (imageInput) {
        swal.fire({
            title: 'Image successfully changed !',
            type: 'success',
            buttonsStyling: false,
            confirmButtonText: 'Awesome!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
    });

    avatar5.on('change', function (imageInput) {
        swal.fire({
            title: 'Image successfully changed !',
            type: 'success',
            buttonsStyling: false,
            confirmButtonText: 'Awesome!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
    });

    avatar5.on('remove', function (imageInput) {
        swal.fire({
            title: 'Image successfully removed !',
            type: 'error',
            buttonsStyling: false,
            confirmButtonText: 'Got it!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
    });

    $('.customercnic , .nomineecnic ').change('change', function () {
        // var idToTest = '12345-1234567-1',
        var checkcinic = $('.customercnic').val();
        var checkcinictwo = $('.nomineecnic').val();
        myRegExp = new RegExp(/\d{5}-\d{7}-\d/);

        if (myRegExp.test(checkcinic)) {
            $('.btn-check').removeAttr('disabled');
            $('.customercnic').removeClass('is-invalid');
        } else {
            $('.customercnic').addClass('is-invalid');
            $('.btn-check').attr('disabled', 'disabled');
        } if (myRegExp.test(checkcinictwo)) {
            $('.btn-check').removeAttr('disabled');
            $('.nomineecnic').removeClass('is-invalid');
        }
        else {
            $('.nomineecnic').addClass('is-invalid');
            $('.btn-check').attr('disabled', 'disabled');
        }
    });

</script>
@endsection
