<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_metas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->text('profile')->nullable();
            $table->string('reg_number')->nullable();
            $table->string('father_name')->nullable();
            $table->string('data_of_birth')->nullable();
            $table->string('gender')->nullable();
            $table->integer('salary')->nullable();
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->string('cell_no')->nullable();
            $table->string('cnic_no')->nullable();
            $table->string('phone_no')->nullable();
            $table->string('status')->nullable();
            $table->string('delete_by')->nullable();
            $table->string('created_by')->nullable();
            $table->enum('delete_at',[0,1])->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_metas');
    }
}
