<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agents', function (Blueprint $table) {
            $table->id();
            $table->text('profile')->nullable();
            $table->string('reg_number')->nullable();
            $table->string('name')->nullable();
            $table->string('father_name')->nullable();
            $table->string('data_of_birth')->nullable();
            $table->string('gender')->nullable();
            $table->integer('comission')->nullable();
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->string('cell_no')->nullable();
            $table->string('cnic_no')->nullable();
            $table->string('phone_no')->nullable();
            $table->string('email')->nullable();
            $table->string('status')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents');
    }
}
