<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plots', function (Blueprint $table) {
            $table->id();
            $table->string('plot_id')->nullable();
            $table->unsignedBigInteger('housing_scheme_id')->nullable();
            $table->string('type')->nullable();
            $table->string('block')->nullable();
            $table->string('phase')->nullable();
            $table->string('width')->nullable();
            $table->string('length')->nullable();
            $table->string('sqrft')->nullable();
            $table->string('sqrmt')->nullable();
            $table->string('marla')->nullable();
            $table->string('cost')->nullable();
            $table->string('totalcost')->nullable();
            $table->string('khewat_no')->nullable();
            $table->string('khasra_no')->nullable();
            $table->string('status')->nullable();
            $table->string('location')->nullable();
            $table->string('address')->nullable();
            $table->string('create_by')->nullable();
            $table->string('delete_by')->nullable();
            $table->enum('delete_at',[0,1])->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plots');
    }
}
